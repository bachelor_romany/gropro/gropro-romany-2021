#!/usr/bin/env python

import sys
import argparse

from src.eingabe    import Eingabe
from src.puzzle     import Puzzle
from src.ausgabe    import Ausgabe

def parse_commandline(argv=sys.argv):
    """
    Argumente Einlesefunktion
    
    Args:
        argv: Liste von Argumenten die aus der Kommandozeile eingelesen werden, default: sys.argv

    Returns:
        list(): Liste aus Argumenten, die mittels argparse generiert wurden
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('pfad', help='Pfad zum Einlesen der Datei',
            type=str)
    # parser.add_argument('-o', '--output_file',
            # help='Optionale Ausgabename einer Datei',
            # nargs='?')
    # parser.add_argument('-w', '--overwrite',
            # help='überschreibt existierende Datei',
            # action='store_true', default=False)

    return parser.parse_args()


def main(argumente):
    """
    Hauptprogramm

    Es ließt die Datei ein, die übergeben wird
    berechnet, falls vorhanden eine Loesung für das Holzpuzzle
    und gibt es anschließend in einer Datei aus, die in
    'output' erzeugt wurde

    Args:
        Liste aus Argumenten, die aus der Konsoleneingabe übernommen werden
    """
    # Datei wird eingelesen

    eingabe = Eingabe(argumente.pfad)
    
    if not eingabe.genuegend_dreiecke_vorhanden():
        print("""
        Fehler in der Eingabedatei.
        Es sind zu viele oder zu wenige Dreicke vorhanden""")
        exit(1)

    # Titel, Dimension und Würfel werden aus der Eingabedatei ausgelesen
    titel       = eingabe.get_titel
    dimension   = eingabe.get_dimension
    wuerfel     = eingabe.get_wuerfel
    
    # Das Spielfeld wird erstellt
    puzzle = Puzzle(dimension, wuerfel)
    
    # Rufe rekursiv den Backtracking Algorithmus auf
    # und speichere das puzzle als geloestes_puzzle
    geloestes_puzzle = \
            Puzzle.loese_puzzle(
                    0,
                    puzzle.get_sortierte_wuerfel,
                    puzzle.create_game())
    
    # Würfel werden die Positionen im Spielfeld zugeordnet
    puzzle.set_loesung(geloestes_puzzle)
    
    # Weise den wuerfel die neuen Würfel zu
    wuerfel = puzzle.get_loesung


    # Erzeuge Ausgabedatei
    ausgabe = Ausgabe(titel, dimension, wuerfel)
    ausgabe.create()


if __name__ == "__main__":
    
    # rufe Methode aus, um Argumente auszulesen
    # Programm bricht ab, wenn kein Argument im Pfad steht
    argumente = parse_commandline()

    main(argumente)
