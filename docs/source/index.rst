.. Matseland documentation master file, created by
   sphinx-quickstart on Thu Apr 15 13:00:26 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Entwickler Dokumentation!
=========================

.. toctree::
    :maxdepth: 4
    
    aufgabenanalyse
    verfahrensbeschreibung
    programmkonzept
    abweichungen
    benutzeranleitung
    entwicklungsumgebung
    entwickleranleitung
    testdokumentation
    ausblick
    erklaerung
