.. _beispiel:

Beispiel 2 aus der Aufgabenstellung
-----------------------------------

Es wird ein Beispiel gezeigt, wie der Algorithmus bei einer Dimension von 1x2x3 funktioniert::

    //************************
    // Beispiel 2
    //************************
    Dimension 1,2,3  
    Teil 1: 0 0 0 2 0 3
    Teil 2: 0 2 0 0 0 2
    Teil 3: 2 2 0 2 0 0
    Teil 4: 0 1 0 1 0 3
    Teil 5: 2 0 0 1 0 0
    Teil 6: 3 1 0 0 0 0


Einlesen aus der Datei
++++++++++++++++++++++

Zunächst wird die Datei zeilenweise eingelesen.
Dies geschieht in einer Schleife.
Die nötigen Informationen werden gespeichert und
es werden eventuelle Fehlerfälle berücksichtigt.

1. Einlesen von Kommentarzeilen für die Erstellung eines Titels und der Ausgabedatei::

    //************************
    // Beispiel 2
    //************************
    Dimension 1,2,3

Hier muss überprüft werden, ob die eingelesene Dimension richtig ist.

2. Einlesen aller Würfel::

    Teil 1: [0, 0, 0, 2, 0, 3]
    Teil 2: [0, 2, 0, 0, 0, 2]
    Teil 3: [2, 2, 0, 2, 0, 0]
    Teil 4: [0, 1, 0, 1, 0, 3]
    Teil 5: [2, 0, 0, 1, 0, 0]
    Teil 6: [3, 1, 0, 0, 0, 0]

Hierbei wird berücksichtigt, ob genug Würfel eingelesen werden.
Bei einer Dimension von 1x2x3 werden also :math:`1 \cdot 2 \cdot 3 = 6`
Würfel benötigt.
Falls ein Würfel keine Dreiecke besitzt oder die Dreicke werden mit einer negativen Zahl
oder größer als 4 gekennzeichnet, bricht das Einlesen ab, und der Benutzer wird
entsprechend der Zeile informiert.

Überprüfung der Dreicke
+++++++++++++++++++++++

Anschließend wird überprüft, ob die Anzahl der leeren Flächen mit der Oberfläche übereinstimmen.
Die Oberfläche lässt sich aus den Dimensionen berechnen 
(:math:`\text{Oberfläche} := 2 \cdot x \cdot y + 2 \cdot x \cdot z + 2 \cdot y \cdot x`)

.. math::

    \text{Oberfläche} := 2 \cdot 1 \cdot 2 + 2 \cdot 1 \cdot 3 + 2 \cdot 2 \cdot 3 &= 22 \\
    \text{Anzahl\ der\ freien\ Flächen\ auf\ den\ Würfeln} &= 22


Vorsortieren der Würfel
+++++++++++++++++++++++

Beim Erzeugen des Puzzles werden die verschiedenen Typen der Würfel
in einem Tuple gespeichert::

    [Teil 1: 0 0 0 2 0 3, Teil 2: 0 2 0 0 0 2, Teil 5: 2 0 0 1 0 0, Teil 6: 3 1 0 0 0 0]
    [Teil 3: 2 2 0 2 0 0, Teil 4: 0 1 0 1 0 3]
    []
    []

- In der ersten Zeile befinden sich die Eckwürfel
- In der zweiten Zeile befinden sich die Kantenwürfel

Da das System nur 2 Dimensionen hat und keine ganze Fläche aufgespannt wird,
existieren keine Flächenwürfel oder Innenwürfel.

Bei einer 3x3x3 Dimension befänden sich in der dritten Zeile die Flächenwürfel und
in der viertel Zeile stünden der Würfel, der im Inneren des Quaders liegt.

Bei einem Puzzle von 5x5x5 würde sich beispielsweise ein Quader aus 3x3x3 im Inneren des 5x5x5 Quaders liegen.
Dadruch würden sich 27 Würfel im inneren befinden.

Anwendung des Algorithmus
+++++++++++++++++++++++++

Nun wird der Algorithmus angewendet. Es wird an die erste Ecke ein Eckwürfel aus den gesamten Eckwürfeln gezogen
und so lange weiter gesucht, bis keine Eckwürfel mehr vorhanden sind.
Gestartet wird mit Position 0.

=== ========== === ======= ===========
pos Koordinate Typ  Würfel   Seiten
=== ========== === ======= ===========
 0   (0, 0, 0)  0  Teil 1: 0 0 0 2 0 3
 1   (0, 1, 0)  0  Teil 2: 0 2 0 0 0 2
 1   (0, 1, 0)  0  Teil 5: 2 0 0 1 0 0
 2   (0, 0, 1)  1  Teil 3: 2 2 0 2 0 0
 2   (0, 0, 1)  1  Teil 4: 0 1 0 1 0 3
 3   (0, 1, 1)  1  Teil 3: 2 2 0 2 0 0
 4   (0, 0, 2)  0  Teil 2: 0 2 0 0 0 2
 5   (0, 1, 2)  0  Teil 6: 3 1 0 0 0 0
=== ========== === ======= ===========

.. figure:: diagramme/beispiel.png
    :align: center
    :width: 200 px

Hier ist leicht zu sehen, wie Teil 2 im zweiten Schritt ersetzt wird durch Teil 5.
Durch vollständige Rotationen war es nicht möglich Teil 2 in das Puzzle zu setzen.
An Position 2 folgt nun ein Kantenwürfel, ersichtlich durch ``Typ 1``.
Die Koordinate liegt zwischen (0, 0, 0) und (0, 0, 2).
Hier wird auch zuerst Teil 3 vollständig duchrotiert, bis dann Teil 4 benutzt wird
und an die Position gesetzt wird.

Der Übersichtlichkeit habe ich alle Rotationen weggelassen.


Zuordnung der Positionen
++++++++++++++++++++++++

Nach Setzen des letzten Würfels auf die letzte Ecke, diagonal gegenüberliegend der nullten
Position ist der Algorithmus durchgelaufen.

Wäre hier keine Lösung ausgegeben worden, wird der Benutzer dementsprechend benachrichtigt.
Nun werden den Würfeln ihre Positionen aus dem errechneten Spielfeld mitgegeben.


Ausgabe der Datei
+++++++++++++++++

Im letzten Schritt werden die Würfel mit ihren Positionen der Ausgabe übergeben
und von ihr in einer Datei gespeichert::

    //************************
    // Beispiel 2
    //************************
    
    Dimension 1,2,3

    [1,1,1] Teil 1: 1 0 0 2 0 0
    [1,1,2] Teil 4: 4 0 0 2 0 4
    [1,1,3] Teil 2: 0 0 0 1 0 1
    [1,2,1] Teil 5: 4 1 0 0 0 0
    [1,2,2] Teil 3: 1 1 0 0 0 1
    [1,2,3] Teil 6: 0 2 0 0 0 4

Hier wird nicht auf die Reihenfolge der Würfel geachtet, sondern
wie die Würfel in das Spielfeld gesetzt werden
damit der Benutzer weiß, wie die Würfel gedreht werden müssen.
Hier werden nicht die Anfangsposition der Dreiecke mitgegeben,
sondern die schon gedrehten Würfel.

Weitere Beispiele
-----------------

Im Folgenden werde ich nur die Unterschiede der Beispiele zueinander erklären
und nicht weiter im Detail auf das Beispiel eingehen.

Beispiel 1
++++++++++

Zu sehen sind zwei Würfelstücke. Bei zwei Würfeln wird immer eine Lösung gefunden.
Es muss hier nur das zweite Würfelstück soweit gedreht werden, dass sich die beiden
Dreiecke ineinander stecken lassen::

    //************************
    // Beispiel 1
    //************************
    Dimension 1, 2, 1
    Teil 1: 0 0 1 0 0 0
    Teil 2: 0 0 0 4 0 0

Lösung::

    //************************
    // Beispiel 1
    //************************
    Dimension 1,2,1
    [1,1,1] Teil 1: 0 0 0 2 0 0
    [1,2,1] Teil 2: 0 1 0 0 0 0

Beispiel 2
++++++++++

Dieses Beispiel wurde am Anfang des Abschnitts vollständig erklärt.
Hier wird jedoch mit einer anderen Dimension getestet::

    //************************
    // Beispiel 2
    //************************
    Dimension 2, 3, 1
    Teil 1: 0 0 0 2 0 3
    Teil 2: 0 2 0 0 0 2
    Teil 3: 2 2 0 2 0 0
    Teil 4: 0 1 0 1 0 3
    Teil 5: 2 0 0 1 0 0
    Teil 6: 3 1 0 0 0 0

Lösung::

    //************************
    // Beispiel 2
    //************************
    Dimension 2,3,1
    [1,1,1] Teil 1: 0 0 3 3 0 0
    [1,2,1] Teil 4: 0 4 1 2 0 0
    [1,3,1] Teil 2: 0 1 2 0 0 0
    [2,1,1] Teil 5: 0 0 0 2 4 0
    [2,2,1] Teil 3: 0 1 0 3 2 0
    [2,3,1] Teil 6: 0 4 0 0 1 0

Zu erkennen ist, dass die Lösung hier unterschiedlich ausfällt im Vergleich zur
Beispielrechnung mit der Dimension 1x2x3.
Das liegt daran, dass die Würfel hier anders rotieren müssen.

Beispiel 3
++++++++++

In diesem Beispiel wir ein Quader aus 2x2x2 generiert.
In diesem Fall kann keine Typisierung stattfinden, da alle Würfel auf den
Ecken liegen.
Somit muss jeder Würfel auf den Ecken durchgetestet werden.

Eingabe::

    //************************
    // Beispiel 3
    //************************
    Dimension 2, 2, 2
    Teil 1: 4 4 2 0 0 0
    Teil 2: 3 4 1 0 0 0
    Teil 3: 3 2 3 0 0 0
    Teil 4: 1 4 4 0 0 0
    Teil 5: 1 4 1 0 0 0
    Teil 6: 3 3 1 0 0 0
    Teil 7: 3 4 2 0 0 0
    Teil 8: 3 2 2 0 0 0

Ausgabe::

    //************************
    // Beispiel 3
    //************************
    Dimension 2,2,2
    [1,1,1] Teil 1: 1 0 3 1 0 0
    [1,1,2] Teil 2: 0 0 2 4 0 4
    [1,2,1] Teil 5: 2 2 2 0 0 0
    [1,2,2] Teil 3: 0 3 3 0 0 3
    [2,1,1] Teil 4: 3 0 0 4 4 0
    [2,1,2] Teil 6: 0 0 0 3 1 2
    [2,2,1] Teil 7: 2 3 0 0 1 0
    [2,2,2] Teil 8: 0 4 0 0 4 3

Beispiel 4
++++++++++

In diesem Beispiel wird eine Fläche erzeugt
mit einer Größe von 3x3.
Hier tritt zum ersten mal in den Beispielen ein Teil auf, welches sich nur in der Mitte
der Fläche platzieren lässt.

Eingabe::

    //************************
    // Beispiel 4
    //************************
    Dimension 3, 3, 1
    Teil 1: 0 0 3 0 1 3
    Teil 2: 2 0 4 0 1 3
    Teil 3: 0 0 0 1 1 0
    Teil 4: 0 3 4 0 0 0
    Teil 5: 0 3 2 0 0 0
    Teil 6: 1 2 0 0 0 2
    Teil 7: 3 2 0 1 0 0
    Teil 8: 0 1 4 0 0 0
    Teil 9: 2 4 0 1 0 0

Hier kann man die Typisierung sehr gut sehen::

    [Teil 3: 0 0 0 1 1 0, Teil 4: 0 3 4 0 0 0, Teil 5: 0 3 2 0 0 0, Teil 8: 0 1 4 0 0 0]
    [Teil 1: 0 0 3 0 1 3, Teil 6: 1 2 0 0 0 2, Teil 7: 3 2 0 1 0 0, Teil 9: 2 4 0 1 0 0]
    [Teil 2: 2 0 4 0 1 3]
    []

Es gibt vier Eckwürfel, vier Kantenwürfel und einen Flächenwüfel.
Da es keine dritte Dimension gibt, bleibt die letzte Liste leer.

Ausgabe::

    //************************
    // Beispiel 4
    //************************
    Dimension 3,3,1
    [1,1,1] Teil 3: 0 0 3 3 0 0
    [1,2,1] Teil 6: 0 4 3 1 0 0
    [1,3,1] Teil 8: 0 2 3 0 0 0
    [2,1,1] Teil 7: 0 0 3 3 4 0
    [2,2,1] Teil 2: 0 4 1 3 4 0
    [2,3,1] Teil 1: 0 4 4 0 4 0
    [3,1,1] Teil 4: 0 0 0 3 4 0
    [3,2,1] Teil 9: 0 4 0 1 2 0
    [3,3,1] Teil 5: 0 2 0 0 3 0

Beispiel 5
++++++++++


In Beispiel 5 ist die Fläche so groß, dass innerhalb des 5x5x1 ein 3x3x1 Feld
Platz hat.
Dadurch vergrößert sich die Anzahl der flächenliegenden Würfel von einem auf
:math:`3\cdot 3 = 9`.

Eingabe::

    //************************
    // Beispiel 5
    //************************
    Dimension 5, 5, 1
    Teil 1: 2 3 0 0 0 4
    Teil 2: 0 2 0 3 3 0
    Teil 3: 1 0 1 0 2 1
    Teil 4: 2 1 0 4 0 3
    Teil 5: 2 0 4 0 4 3
    Teil 6: 0 4 0 0 0 3
    Teil 7: 2 3 0 0 0 1
    Teil 8: 4 2 0 2 0 4
    Teil 9: 0 3 4 3 4 0
    Teil 10: 1 1 0 2 0 3
    Teil 11: 1 0 1 0 1 0
    Teil 12: 1 2 0 1 0 1
    Teil 13: 0 2 0 0 0 2
    Teil 14: 1 0 0 0 4 2
    Teil 15: 0 4 0 3 4 0
    Teil 16: 3 0 0 2 0 3
    Teil 17: 4 4 0 4 0 0
    Teil 18: 1 0 4 0 3 3
    Teil 19: 2 0 0 0 1 1
    Teil 20: 2 0 0 0 1 0
    Teil 21: 2 4 0 0 0 4
    Teil 22: 3 0 4 0 3 3
    Teil 23: 3 0 0 0 4 3
    Teil 24: 4 0 4 0 0 0
    Teil 25: 3 1 0 4 0 0

Ausgabe::

    //************************
    // Beispiel 5
    //************************
    Dimension 5,5,1
    [1,1,1] Teil 6: 0 0 1 1 0 0
    [1,2,1] Teil 11: 0 2 4 4 0 0
    [1,3,1] Teil 16: 0 3 1 1 0 0
    [1,4,1] Teil 21: 0 2 1 2 0 0
    [1,5,1] Teil 24: 0 1 1 0 0 0
    [2,1,1] Teil 1: 0 0 2 4 2 0
    [2,2,1] Teil 5: 0 3 4 1 3 0
    [2,3,1] Teil 4: 0 2 3 1 2 0
    [2,4,1] Teil 3: 0 2 2 1 2 0
    [2,5,1] Teil 15: 0 2 1 0 2 0
    [3,1,1] Teil 2: 0 0 4 1 1 0
    [3,2,1] Teil 8: 0 2 1 4 3 0
    [3,3,1] Teil 12: 0 3 3 1 4 0
    [3,4,1] Teil 9: 0 2 1 2 1 0
    [3,5,1] Teil 23: 0 1 2 0 2 0
    [4,1,1] Teil 14: 0 0 2 3 3 0
    [4,2,1] Teil 18: 0 4 1 2 2 0
    [4,3,1] Teil 22: 0 1 4 2 4 0
    [4,4,1] Teil 10: 0 1 1 1 2 0
    [4,5,1] Teil 17: 0 2 3 0 1 0
    [5,1,1] Teil 20: 0 0 0 2 1 0
    [5,2,1] Teil 7: 0 1 0 4 2 0
    [5,3,1] Teil 25: 0 3 0 2 3 0
    [5,4,1] Teil 19: 0 1 0 4 2 0
    [5,5,1] Teil 13: 0 3 0 0 4 0
