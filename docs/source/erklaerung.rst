Erklärung des Prüfungsteilnehmers
=================================

Ich erkläre verbindlich, dass das vorliegende Prüfprodukt von mir selbstständig erstellt
wurde. Die als Arbeitshilfe genutzten Unterlagen sind in der Arbeit vollständig aufgeführt.
Ich versichere, dass der vorgelegte Ausdruck mit dem Inhalt des von mir erstellten digitalen Version identisch ist.
Weder ganz noch in Teilen wurde die Arbeit bereits als Prüfungsleistung vorgelegt.
Mir ist bewusst, dass jedes Zuwiderhandeln als Täuschungsversuch zu gelten hat,
der die Anerkennung des Prüfprodukts als Prüfungsleistung ausschließt.

.. raw:: latex

    \hspace{1cm}

.. figure::  diagramme/sign.png
   :width: 500px

.. raw:: latex
    
    \begin{tabbing}
    \rule{7cm}{0.4pt}\=\hspace*{4cm}\=\rule{5cm}{0.4pt}\\
    \small{Ort und Datum}\>\>\small{Unterschrift des Prüfungsteilnehmers}
    \end{tabbing}

