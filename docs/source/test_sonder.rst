Sonderfälle
-----------

Ein Würfel in einer Dimenion
++++++++++++++++++++++++++++

Einen Sonderfall stellt das Spiel mit nur einem Würfel ohne Dreiecke dar.

Eingabe::

    //************************
    // Sonderfall
    //************************
    Dimension 1,1,1
    Teil 1: 0 0 0 0 0 0


Ausgabe::

    Spezialfall 1x1x1, es existiert nur ein Würfel, Lösung ist trivial.
