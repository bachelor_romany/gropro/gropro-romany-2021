Abweichungen vom Vorentwurf
===========================

Einige Variablen und Modulnamen wurden angepasst, um dem Standard in Python zu entsprechen.
Die Methode ``to_string()`` wird in Python mit der Methode ``__str__()`` realisiert.

Außerdem gibt es in Python nicht die Möglichkeit, die Sichtbarkeit so stark einzuschränken,
wie es in anderen objektorientierten Programmiersprachen der Fall ist.
In Python kann man Attribute und Operationen mit dem vorangestelltem Unterstrich als *intern* markieren.
Das bedeutet, dass Nutzer der API solche Attribute und Operationen nicht benutzen sollten,
verhindert wird das aber nicht.
Darum vereinfacht sich der Aufbau mit Gettern und Settern und den Paketen deutlich.
Attribute können beispielsweise als *intern* markiert werden
und trotzdem kann von anderen Klassen darauf zugegriffen werden.
Das habe ich aber vermieden, indem ich für besondere Attribute, die andere Klassen benutzen,
Getter Methoden geschrieben habe.

Einige Algorithmen wurden etwas vereinfacht, da es in Python zum Beispiel die Möglichkeit gibt Tuple zu erstellen,
habe ich zum Erstellen der Dimension, nicht extra eine Klasse geschrieben.

Die Überprüfung auf einen Überlauf entfällt in Python, da Integer automatisch beliebig groß werden können.

In einigen Fällen wurden kleinere Hilfsattribute hinzugefügt, sowie lineare Programmteile in Funktionen ausgelagert.
Das dient nur der Übersicht und hat sonst keinen Einfluss auf den Algorithmus.

Die Methode des Würfels ``get_nachbarn()`` wurde verworfen, da sie nur die hinteren,
seitlichen und unteren Koordinaten ausgegeben hat.
Zudem setze ich erst am Ende des Algorithmus die Koordinaten eines Würfels, so hat er beim einsetzen in dem Quader
noch gar keine Position erfasst.
