Benutzeranleitung
=================

Environment Installation
------------------------

Damit das Programm ordnungsgemäß funktioniert, muss erst eine Python Umgebung erzeugt werden.
Voraussetzung hierfür ist, dass `python 3` ab einer Version 3.7 auf einem Linux System installiert ist.
Zusätzlich, um das Environment zu installieren ist es nötig, dass python3 venv installiert ist.

Um die Installation der Umgebung zu erstellen, muss mit einem shell Befehl im :mod:`Root` Verzeichnis die Installation gestartet werden.

    ``./install.sh``

Das Skript installiert ins :mod:`Root` Verzeichnis die Umgebungsvariablen mit den zugehörigen Abhängigkeiten,
die das Projekt benötigen und erstellt ein Verzeichnis ``holzpuzzle``.

Möchte man der Umgebung einen anderen Namen geben kann das Skript mit einem weiteren Argument aufgerufen werden.

    ``./install.sh <name_der_umgebung>``

Um die Umgebung nutzen zu können, muss sie aktiviert werden

    ``source holzpuzzle/bin/activate``

oder entsprechend
    
    ``source <name_der_umgebung>/bin/activate``

Programmaufruf
--------------

Das Programm wird gestartet,
indem das Hauptskript :mod:`main` mit einem Python Interpreter aufgerufen wird.
Durch das Setzen der Umgebungsvariablen nutzt der Interpreter ``holzpuzzle/bin/python3`` welches mit der Umgebung installiert worden ist.
Hier kann das Programm direkt mit ``./main.py`` gestartet werden.
Andernfalls muss man den Python-Interpreter explizit angeben: ``python3 main.py``.

Zur Ausführung des Programms wird eine Eingabedatei benötigt.
Standardmäßig wird diese in Form eines Kommandozeilen-Parameters übergeben, der den Pfad zu einer Eingabedatei beschreibt.
Die Beispieldateien liegen im Ordner ``./input``. Es ist egal, wo die Dateien liegen, da das Programm immer relativ zum :mod:`Root`
Verzeichnis aufgerufen wird.

Ein Aufruf der Datei ``Beispiel 1-1.txt``::

	./main.py Beispiel\ 1-1.txt

Das Leerzeichen wird hier maskiert.

Die Ausgabedatei wird so benannt, wie sie in der zweiten Zeile der Kommentarzeile eingefügt wurde
und im Ordner ``./output`` angelegt.

Im Abschnitt :ref:`beispiel` wird ein Beispiel durchgerechnet.


Mögliche Fehler
---------------

Zu viele oder keine Parameter
+++++++++++++++++++++++++++++

Das Programm nimmt genau einen Parameter entgegen. Möglicherweise verursacht ein unmaskiertes Leerzeichen im Parameter, dass aus einem Parameter ungewollt zwei werden.
Mit dem Befehl ``python3 main.py -h`` wird die Hilfe aufgerufen. Sie beschreibt wie die Datei einzulesen ist::

    usage: main.py [-h] pfad

    positional arguments:
        pfad        Pfad zum Einlesen der Datei

    optional arguments:
        -h, --help  show this help message and exit




Die Datei kann nicht geöffnet werden
++++++++++++++++++++++++++++++++++++

Entweder der angegebene Pfad existiert nicht oder die Datei beziehungsweise ein übergeordnetes Verzeichnis hat beschränkte Zugriffsrechte.


Formatfehler
++++++++++++

Fehler im Eingabeformat gibt es viele, Erklärungen dazu können in Kapitel :ref:`testen` gefunden werden.
