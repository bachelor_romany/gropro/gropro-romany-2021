Verfahrensbeschreibung
======================

Einlesen der Daten
------------------

Nachdem fehlerfreien Einlesen, werden die
leeren Flächen der Würfel und die freien Flächen auf dem Quader verglichen.
Die Oberfläche lässt sich mit

.. math::

    O = 2 \cdot x \cdot y + 2 \cdot x \cdot z + 2 \cdot y \cdot z 

berechnen.
Beim Einlesen werden die freien Flächen mit gezählt, um nicht über alle
verfügbaren Würfel zu iterieren.

.. figure:: diagramme/oberflaeche.png
    :align: center
    :width: 300 px

In diesem Beispiel gibt es dementsprechend 

.. math::

    2 \cdot 4 \cdot 3 + 2 \cdot 4 \cdot 2 + 2 \cdot 3 \cdot 2 = 52 \text{ Oberflächen}

Durch Prüfen der Oberfläche und den leeren Flächen,
die von den Würfeln eingelesen werden, lässt sich früh erkennen,
ob zu viele oder zu wenige Dreicke im Puzzle liegen und
somit keine Lösung errechnet werden kann.

Eigenschaften des Würfels
-------------------------

Ein Würfel besteht aus sechs Seiten, die jeweils eine Zahl behinhalten.
Die Zahl steht für die Ausrichtung des jeweiligen Dreiecks oder der
glatten Fläche.

Der Würfel hat die Möglichkeit sich in den drei Raumachsen frei zu bewegen.
Dafür wurden die folgenden Richtungen betrachtet:

- drehe um die X Achse um -90°
- drehe um die Y Achse um -90°
- drehe um die Z Achse um +90°

Dabei steht das + und - jeweils für die mathematische Drehrichtung um die
jeweilige Achse.


.. figure:: diagramme/rotationen.png
    :align: center
    :width: 500 px

In der Abbildung wird gezeigt, wie sich die Orientierungen der Dreiecke
ändert, wenn auf allen Flächen das Dreieck in die untere linke Ecke zeigt.

Es muss nur noch darauf geachtet werden, ob sich auf einer vorherigen
Fläche kein Dreieck befindet.

Dreiecke lassen sich zusammenstecken
------------------------------------

Um zu Überprüfen, ob sich Würfel gegenüber stehen, müssen sich deren Dreiecke ineinander einfügen lassen.
Dies muss nur mit drei Nachbarn verglichen werden, da immer gleich aufgefüllt wird, müssen die jeweils hinteren
Nachbarn auf der X und Y Achse und der untere Nachbar auf der Z Achse geprüft werden.

.. figure:: diagramme/lass_stecken.png
    :align: center
    :width: 200px

Komplexitätsreduzierung
-----------------------

Aufgrund der hohen Kombinationsmöglichkeiten die ein Würfel an einer
Position hat, werden die Würfel beim übergeben in das Puzzle typisiert.
Das heißt es gibt Würfel für:
    
- Ecken,
- Kanten,
- Oberflächen,
- und das Innere des Spielfeldes.

Durch das Typisieren muss nicht jeder Würfel an jeder Stelle ausprobiert
werden. Hierdurch reduziert sich die anfänglich erwähnten :math:`n!` Permutationen.

.. figure:: diagramme/wuerfel_typen.png
    :align: center
    :width: 300 px

Beispiel der Typisierung für einen Quader mit 2x2x2 Dimension::
    
    (2, 2, 2)
    Ecken
    Teil 1: 4 4 2 0 0 0
    Teil 2: 3 4 1 0 0 0
    Teil 3: 3 2 3 0 0 0
    Teil 4: 1 4 4 0 0 0
    Teil 5: 1 4 1 0 0 0
    Teil 6: 3 3 1 0 0 0
    Teil 7: 3 4 2 0 0 0
    Teil 8: 3 2 2 0 0 0

Hier ist zu sehen, dass ein Quader mit acht Würfeln nur aus
Ecken besteht.


Beispiel der Typisierung für einen Quader mit 3x3x3 Dimension::

    (3, 3, 3)
    Ecken
    Teil 2: 0 3 3 0 0 3
    Teil 5: 0 0 1 2 0 1
    Teil 8: 0 4 2 0 0 1
    Teil 9: 0 0 0 3 3 1
    Teil 11: 3 0 0 4 3 0
    Teil 20: 0 2 3 0 0 1
    Teil 21: 0 2 2 0 0 4
    Teil 23: 2 0 2 1 0 0
    Kanten
    Teil 1: 0 3 0 1 2 3
    Teil 4: 0 1 3 4 0 3
    Teil 7: 4 3 2 0 0 1
    Teil 10: 2 3 0 3 4 0
    Teil 13: 3 3 0 3 2 0
    Teil 15: 0 3 1 1 0 3
    Teil 17: 0 4 2 0 1 1
    Teil 18: 4 3 4 0 3 0
    Teil 22: 4 0 1 3 0 3
    Teil 24: 0 3 2 1 0 1
    Teil 25: 0 1 0 1 2 2
    Teil 27: 3 3 0 0 4 4
    Flaechen
    Teil 3: 4 3 2 0 1 4
    Teil 6: 4 0 4 1 1 4
    Teil 14: 4 4 0 4 3 1
    Teil 16: 3 3 0 2 3 2
    Teil 19: 3 3 1 4 4 0
    Teil 26: 2 3 3 1 0 2
    Innen
    Teil 12: 4 1 4 2 3 3

Wie in diesem Beispiel zu sehen ist, gibt es in einem Quader
der Größe 3x3x3 acht Ecken
(wie auch in jedem weiteren größeren Quader) und
einen Würfel, der genau im Mittelpunkt des Quaders liegt.


Algorithmus
-----------

Für das Lösen des Puzzles wurde ein rekursives Verfahren mit
Backtracking gewählt.
Hierzu wird in einer Ecke angefangen, vorzugsweise an den Koordinaten
(1,1,1), also dem Koordinatenursprung.
Ein passender Würfel, der für diese Position
genutzt werden soll, wird an diese Stelle platziert.
Die Orientierung der Dreiecke des Würfels werden überprüft und mit möglichen
Nachbarn verglichen. Wenn der Würfel so passt, wird ein nächster Würfel 
der passenden Kategorie platziert, bis es gelöst ist.
Passt ein Würfel nicht direkt
in das Spielfeld, wird er über seine Achsen gedreht und weiter geprüft.
Konnte ein Würfel nach allen Rotationen nicht platziert werden,
wird er aus dem Spielfeld genommen
und ein nächster möglicher Würfelkandidat an diese Stelle gesetzt.
Das Verfahren wird so weiter geführt, bis die letzte Ecke platziert wurde.

.. figure:: diagramme/algorithmus.png
    :align: center
    :width: 300 px

Wurden alle Würfel gesetzt, ist das Puzzle gelöst und die Würfel können
ausgegeben werden.
Falls dieses Verfahren über alle möglichen Ecken keine Lösungen findet,
wird der Benutzer lediglich darauf hingewiesen, dass keine Lösung existiert.

Spielfeld/Gitter
----------------

Um sich im Spielfeld besser zu orientieren,
wir dem Algorithmus stets die nächste Postition, die noch freien Würfel
und das eigentliche Spielfeld übergeben.
So ist es einfacher die Position um eins zu erhöhen, als darauf zu achten,
in welchen Kooridnaten mein nächster
Würfel liegt.
Die Position wird in dreidimensionale Koordinaten umgerechnet, damit der Würfel mit seinen
direkten Nachbarn verglichen werden kann.

Zur besseren Performance wird die Größe des Spielfeldes iterativ berechnet,
da sonst immer über drei Schleifen iteriert werden müsste.

.. math::

    \text{Z Koordinate} &= \text{Ganzzahldivision von } \text{Position und} \\
    & (\text{Größe der Dimension in X Richtung} \cdot \text{Größe der Dimension in Y Richtung}) \\
    z &= int\left(\frac{pos}{(x.shape \cdot y.shape)}\right) \\
    \text{Rest der Z Koordinate} &= \text{Modulo/Rest } \text{Position von} \\
    & (\text{Größe der Dimension in X Richtung} \cdot \text{Größe der Dimension in Y Richtung}) \\
    yx &= pos\text{ mod}(x.shape \cdot y.shape) \\
    \text{Y Koordinate} &= \text{Ganzzahldivision von } \text{yx und} \\
    & (\text{Größe der Dimension in X Richtung} \\
    y &= int\left(\frac{yx}{x.shape}\right) \\
    \text{X Koordinate} &= \text{Modulo/Rest von } \text{yx und} \\
    & (\text{Größe der Dimension in X Richtung}) \\
    x &= yx\text{ mod}(x.shape)
