Aufgabenanalyse
===============


Aufgabenbeschreibung
--------------------

Die Matse Spaß AG möchte einen Algorithmus haben, um ein Holzpuzzle,
welches aus Würfeln mit aufgesetzten Dreiecken besteht, zu lösen.

.. figure::  diagramme/holzpuzzle.jpg
   :align:   center
   :width: 120px

Quelle: `https://i.otto.de/i/otto/16d57925-aa17-5ee3-94a8-dec6b65a31fe`

Diese Würfel befinden sich innerhalb eines Quaders,
welcher sich mit der Dimension <x>, <y> und <z> beschreiben lässt.

Um einen Würfel in den Quader zu platzieren,
darf sich kein Dreieck eines Würfels auf der Außenseite des Quaders befinden.
Die Würfelseiten die innerhalb des Quaders liegen,
müssen ein Dreick behinhalten.
Somit treffen sich immer zwei Dreicke im inneren des Quaders.
Damit der Würfel dort platziert werden darf,
müssen die beiden Dreicke so ineinander gedreht sein, dass daraus ein Quadrat
entsteht.
Ein Würfel kann nur so mit einem anderen Würfel zusammen gesteckt werden,
die jeweils ein Dreieck auf den gegenüberliegenden Flächen haben,
siehe folgender Skizze.

.. figure::  diagramme/zusammen_stecken.png
    :align: center
    :width: 120 px

Die Fläche der Holzdreiecke auf den Würfeln ist blau eingefärbt.
Um die vier Richtungen auf den Würfeln darzustellen,
werden sie mit den Zahlen 0 - 4 eindeutig identifiziert.

.. raw:: latex

    \newpage

.. figure:: diagramme/dreiecke.png
    :align: center
    :width: 300 px

============ ============ ============ ============ ============
     0            1            2            3            4
kein Dreieck  unten links  oben links   oben rechts unten rechts
============ ============ ============ ============ ============

Die '0' bedeutet, dass sich kein Dreieck auf dem Würfel
befindet.

Desweiteren werden die sechs Seiten eines Würfels folgendermaßen definiert,
siehe Skizze.

.. figure:: diagramme/wuerfel_seiten.png
    :align: center
    :width: 300 px

Der Würfel ist stets so ausgerichtet,
dass Seite 1 nach oben und Seite 3 nach vorne zeigt.

Darüber hinaus lässt sich ein Würfel beliebig über die drei
Achsen X, Y und Z drehen.

Um die Komplexität zu bestimmen, wie viele Möglichkeiten es gibt
einen Würfel in einem Quader mit :math:`n` Teilwürfeln unterzubringen,
wird die Anzahl der Rotationen errechnet.
Da es vier Drehungen mit sechs Seiten gibt,
kann ein Würfel mit :math:`m = 6 \cdot 4 = 24` Möglichkeiten
auf einer Position gedreht werden.
Um :math:`n` Würfel an :math:`n` Plätze zu positionieren, kann das
Problem abstrahiert werden, indem berechnet wird,
dass :math:`n` unterscheidbare Würfel, auf :math:`n` Plätze in einer
Reihe nebeneinander angeordnet werden.
Für den ersten Würfel gibt es :math:`n`
Platzierungsmöglichkeiten. Für den zweiten Würfel gibt es
:math:`(n-1)` Möglichkeiten, für den dritten Würfel verbleiben :math:`(n-2)`
Möglichkeiten und für den letzten Würfel verbleibt nur noch eine
mögliche Position.
Mathematisch beschreibt das Problem eine Permutation ohne Wiederholung.

.. math::

   n \cdot (n - 1) \cdot (n - 2) \cdot ... \cdot 1 = n!

Durch die große Anzahl an Permutationen, die ein Würfel innerhalb
des Quaders annehmen kann, wird versucht, diese zu verkleinern.

Möglichkeiten aus :math:`n` Würfeln mit :math:`n` Teilwürfeln:

.. math::
    
    k(n) = n! \cdot 24^n

Im folgenden wird ein Algorithmus erstellt, der für jede
Dimension eine Lösung ausgibt.

.. raw:: latex

    \newpage

Module
------

Das Problem lässt sich in folgende Module zerlegen:

#. Einlesen der Eingabedatei
#. Erstellen des Puzzle (Holzspiel)
    
    #. Ermitteln der Dimension und erstellen eines Gitters
    #. Vorsortieren der einzelnen Würfeltypen
    #. Anwenden des Algorithmus
    #. Würfel Positionen im Gitter zuweisen

#. Ausgabe der Daten

Randbedinungen
--------------

Damit das Puzzle eine geeignete Gitterstruktur erzeugen kann,
müssen einige Bedingungen erfüllt sein:

- Es dürfen nicht zu wenige oder zu viele Würfel in der
  eingelesenen Datei mitgegeben werden.
- Eine Dimension darf nicht :math:`0` sein.
- Die Würfel dürfen nur mit den Zahlen 0 bis 4 angegeben werden,
  da sonst die Richtung der Dreiecke nicht eindeutig bestimmt
  werden kann.
- Es sollte zu Beginn geprüft werden, ob genügend Dreiecke,
  beziehungsweise eine gerade Anzahl an Dreiecken übergeben wurde.

Eingabe
-------

Die Eingabe erfolgt über eine Datei, aus der der Name des zu
lösenden Puzzles und die einzelnen Würfel ausgelesen werden.

Die Datei besteht aus Kommentarzeilen, dem Titel, der Dimension und
den Würfeln, mit den aufgesetzten Dreiecken.
Innerhalb der Kommentarzeilen wird der Titel des Puzzles
beschrieben.
Die Dimension steht direkt unterhalb der Kommentarzeilen und
darunter werden die Würfel mit Teilnummer aufgelistet.

Kommentarzeilen beginnen mit ``//`` und sind bis auf den Titel
irrelevant für das Programm.
Die Dimension wird kommasepariert eingegeben.

Beispiel einer Eingabedatei::


    //************************
    // Titel des Puzzles
    //************************
    Dimension <X>,<Y>,<Z>
    Teil 1: 1 2 3 0 0 0
    Teil 2: 4 3 2 0 0 0
    ...
    Teil n: 1 1 1 0 0 0

Die Würfel werden fortlaufend hochgezählt und mit einem
Leerzeichen werden die Zahlen der Richtungen der Dreiecke
eingetragen.

Es ist davon auszugehen, dass die Syntax immer korrekt ist,
so muss dieses nicht geprüft oder getestet werden.

Ausgabe
-------

Die Ausgabe besteht aus den Kommentarzeilen, die aus der Eingabe
entnommen wurden. Sie behinhaltet zudem die Positionen der Würfel,
die der Algorithmus bestimmt hat, mit denen das Puzzle gelöst werden kann.

Beispiel einer Ausgabedatei::

    //************************
    // Titel des Puzzles
    //************************
    [1,1,2] Teil 1: 1 2 3 0 0 0
    [1,2,1] Teil 2: 4 3 2 0 0 0
    ...
    [x,y,z] Teil n: 1 1 1 0 0 0
