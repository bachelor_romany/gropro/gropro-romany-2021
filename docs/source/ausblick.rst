Zusammenfassung und Ausblick
============================


Der Backtracking Algorithmus kann auch verwendet werden um alle Lösungen zu
finden.
In dem entwickelten Programm wird aber nach dem Finden der ersten Lösung abgebrochen und die richtige
Lösung wird ausgegeben, sofern eine vorhanden ist.

Momentan arbeitet der Algorithmus noch sequenziell. Um den Backtracking Algorithmus
zu parallelisieren kann nicht viel während des Vorgangs parallelisiert werden.
Aber es kann parallelisiert werden, in dem zum Beispiel die Eckwürfel parallel
eingelesen werden. Je nachdem wie viele Ecken das Puzzle hat und oder wie
viele Threads dem Computer zur Verfügung stehen.

Desweiteren könnte die Komplexität weiter reduziert werden, in dem die Rotation der
Würfel weiter betrachtet wird, Ein Eckwürfel in Dimension 3x3x3, hat nur 2 Möglichkeiten
sich in dieser Ecke platzieren zu lassen.
So kann eingegrenzt werden, dass dich Würfel generell nicht nach außen drehen lassen können,
falls an dieser Stelle ein Dreieck sitzt.


