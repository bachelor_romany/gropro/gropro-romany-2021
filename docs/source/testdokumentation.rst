.. _testen:

Testdokumentation
=================

Um die Korrektheit des Programms zu Testen wurde ein Whitebox-Test mit
allen Bedingungsüberdeckungen angewendet.
Die jeweiligen Test Dateien sind im Ordner ``./tests`` zu finden.

Die Sonder- und Fehlerfälle sind in ``./input/`` unter ``sonder`` und ``fehler`` zu finden.

Diese werden mit dem Befehl ``./testen.sh`` aus dem Hauptverzeichnis aufgerufen.

Es werden alle Tests in diesem Verzeichnis ausgeführt.
Bei jeder Datei wird zu Beginn aufgerufen, was getestet wird und am Ende, wenn der Test erfolgreich war::

    TEST OK

.. toctree::
    ihk_beispiel.rst
    test_sonder.rst
    test_fehler.rst

