Entwicklungsumgebung
====================

Das Programm wurde auf einem *Lenovo Thinkpad T14* mit **Intel(R) Core(TM) i7-10510U (8)** CPU
und **32 GB RAM** entwickelt.

Das Betriebssystem ist **Arch Linux** mit einem *Standard Python Interpreter* der Version **Python 3.9.5**.

Weitere Hilfsprogramme sind der Texteditor **neovim** und die Internetseite **draw.io**
für das Erstellen der UML-Diagramme und **Sphinx** mit *napoleon* für das erstellen dieses Dokuments
und insbesondere des Abschnitts :ref:`Entwickleranleitung`.
