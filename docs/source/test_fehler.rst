Fehlerfälle
-----------

Abschließend wird das Verhalten bei ungültigen Eingaben untersucht.
Dabei wird vorausgesetzt,
dass die Eingabedatei geöffnet werden konnte.
Die Ausgabe findet auf der Konsole statt,
eine Ausgabedatei wird nicht erzeugt.


Anzahl Würfel ist größer als die Dimension
++++++++++++++++++++++++++++++++++++++++++

Hier sind mehr Würfel in der Datei erhalten,
als die Dimension zulässt.

Eingabe::

    //********************************
    // Mehr Wuerfel als Dimensionen
    //********************************
    Dimension 1,1,4
    Teil 1: 0 0 1 0 0 0
    Teil 2: 0 0 0 4 0 0
    Teil 3: 0 0 1 0 1 0
    Teil 4: 2 0 0 3 0 0
    Teil 5: 1 0 0 0 0 0

Ausgabe::

    Zeile 9 Fehler in Teil 5: mehr Würfel als Dimensionen?



Anzahl Würfel ist kleiner als die Dimension
+++++++++++++++++++++++++++++++++++++++++++

Hier sind weniger Würfel in der Datei erhalten,
als die Dimension beschreibt.

Eingabe::

    //********************************
    // Weniger Wuerfel als Dimensionen
    //********************************
    Dimension 1,1,4
    Teil 1: 0 0 1 0 0 0
    Teil 2: 0 0 0 4 0 0
    Teil 3: 0 0 1 0 1 0

Ausgabe::

    Weniger Würfel als Dimensionen?

Es sind zu viele Dreiecke auf den Würfeln
+++++++++++++++++++++++++++++++++++++++++

Hier wird getestet, wenn ein System zwar die richtige Anzahl an Würfeln übergeben bekommt,
aber die Würfel haben insgesamt mehr Dreiecke aufgesetzt
als Verbindungsstücke vorhanden wären.

Eingabe::

    //********************************
    // Zu viele Dreiecke
    //********************************
    Dimension 1,1,4
    Teil 1: 0 0 1 0 0 0
    Teil 3: 0 0 1 0 1 0
    Teil 4: 2 0 0 0 0 2
    Teil 2: 0 0 0 4 0 1

Ausgabe::

    Fehler in der Eingabedatei.
    Es sind zu viele oder zu wenige Dreicke vorhanden

Es sind zu wenige Dreiecke auf den Würfeln
++++++++++++++++++++++++++++++++++++++++++

In diesem Beispiel sind weniger Dreicke vertreten
als es Schnittkanten zueinander gibt.

Eingabe::

    //********************************
    // Zu wenig Dreiecke
    //********************************
    Dimension 1,1,4
    Teil 1: 0 0 1 0 0 0
    Teil 3: 0 0 1 0 1 0
    Teil 4: 2 0 0 0 0 0
    Teil 2: 0 0 0 4 0 0

Ausgabe::

    Fehler in der Eingabedatei.
    Es sind zu viele oder zu wenige Dreicke vorhanden



Würfel hat eine falsche Orientierung, negative Zahl
+++++++++++++++++++++++++++++++++++++++++++++++++++

In diesem Test wird gezeigt, dass ein Dreieck keine negative
Ausrichtung haben darf.

Eingabe::

    //********************************
    // Negative Zahl
    //********************************
    Dimension 1,1,4
    Teil 1: 0 0 1 0 0 0
    Teil 2: 0 0 0 -4 0 0
    Teil 3: 0 0 1 0 1 0
    Teil 4: 2 0 0 0 0 0

Ausgabe::

    Zeile 6 Fehler in Teil 2: Dreieck < 0?

Würfel hat eine falsche Orientierung, Zahl > 4
++++++++++++++++++++++++++++++++++++++++++++++

Eine Ausrichtung eines Dreiecks darf nicht 
größer als 4 sein.

Eingabe::

    //********************************
    // Groesser Vier
    //********************************
    Dimension 1,1,4
    Teil 1: 0 0 1 0 0 0
    Teil 2: 0 0 0 4 0 0
    Teil 3: 0 0 1 0 1 0
    Teil 4: 2 0 0 5 0 0

Ausgabe::
    
    Zeile 8 Fehler in Teil 4: Orientierung > 4?

Eine Dimension ist 0
++++++++++++++++++++

Eine eingelesene Dimension hat keine Dimension,
also Dimension 0.

Eingabe::

    //************************
    // Dimension 0
    //************************
    Dimension 0,2,2
    Teil 1: 4 4 2 0 0 0
    Teil 2: 3 4 1 0 0 0
    .
    .
    .

Ausgabe::
    
    Zeile 4 Fehler in der Dimension, Dimension 0?

Eine Dimension ist negativ
++++++++++++++++++++++++++

Eine eingelesene Dimension hat eine negative Dimension,
also Dimension < 0.

Eingabe::

    //************************
    // Dimension 0
    //************************
    Dimension 2,-1,2
    Teil 1: 4 4 2 0 0 0
    Teil 2: 3 4 1 0 0 0
    .
    .
    .

Ausgabe::
    
    Zeile 4 Fehler in der Dimension, Dimension < 0?


Puzzle ist nicht lösbar
+++++++++++++++++++++++

Zuletzt wird ausgegeben, wenn eine Datei nicht
lösbar ist.

Eingabe::

    //********************************
    // nicht loesbar
    //********************************
    Dimension 1,1,4
    Teil 1: 0 0 1 0 0 0
    Teil 3: 0 0 1 0 0 1
    Teil 4: 2 0 0 0 0 2
    Teil 2: 0 0 0 3 0 0

Ausgabe::
    
    Es gibt keine Lösung
    //********************************
    // nicht loesbar
    //********************************

    Dimension 1,1,4
