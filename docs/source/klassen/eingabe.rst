Klasse Eingabe
--------------

.. automodule:: eingabe
   :members:
   :private-members:
   :undoc-members:
   :show-inheritance:

.. literalinclude:: ../../../src/eingabe.py
