Puzzle
------

.. automodule:: puzzle
   :members:
   :private-members:
   :undoc-members:
   :show-inheritance:

.. literalinclude:: ../../../src/puzzle.py
