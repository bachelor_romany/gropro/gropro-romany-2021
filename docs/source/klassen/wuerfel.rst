Würfel
------

.. automodule:: wuerfel
   :members:
   :private-members:
   :undoc-members:
   :show-inheritance:

.. literalinclude:: ../../../src/wuerfel.py
