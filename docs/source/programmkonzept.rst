Programmkonzept
===============

Klassendiagramm
---------------

.. figure::  diagramme/klassendiagramm.png
   :align:   center

.. raw:: latex

    \newpage

Sequenzdiagramm
---------------

Das Programm arbeitet nach dem EVA-Prinzip (Eingabe, Verarbeitung, Ausgabe).
Nachdem der Benutzer das Programm startet, beginnt das Einlesen der Datei
in der Klasse *Eingabe* und die Würfel werden vorbereitet.
Wenn die *Eingabe* erfolgreich war, wird das Spielfeld mit der Klasse
*Puzzle* generiert. Hier wird das Feld erstellt und über *loese_puzzle()*
wird das Puzzle mit dem übergebenen Feld und den Würfeln gelöst.
Das Hauptprogramm bekommt das Feld mit den rotierten Würfeln
und deren Positionen im Feld zurück.
Die Klasse *Ausgabe* kann mit diesen Informationen die Datei mit der
Ausgabe erstellen.


.. figure:: diagramme/sequenzdiagram.png
    :align: center

.. raw:: latex

    \newpage

Nassi-Shneidermann-Diagramme
----------------------------

loese_puzzle()
++++++++++++++

.. figure:: diagramme/loese_puzzle.png
    :align: center


Klasse Eingabe und Ausgabe
++++++++++++++++++++++++++

Die Ein- und Ausgabe funktioniert wie in der Aufgabenanalyse beschrieben.
Die Klasse *Eingabe* liest die übergebene Datei zeilenweise ein, dabei
wird vorausgesetzt, dass die *Input*-Datei eine korrekte Syntax enthält.
Beim Einlesen der Dimension und der Würfel wird auf Fehlerfälle geachtet, 
hierzu mehr im Kapitel der Testdokumentationen.

Die Klasse *Eingabe* besteht aus folgenden Attributen:

- _dimension         (Tuple(int, int int)) : Dimension des Puzzles
- _wuerfel_liste     (Liste aus Würfeln) : Speichert die eingelesenen Würfel
- _titel             (String) : Titel der Eingabedatei
- _input             (String) : Pfad der Eingabedatei
- _oberflaeche       (int) : Wie groß die Oberfläche der Dimension ist
- _leere_flaechen    (int) : Wie viele leere Flächen aus den Würfeln ausgelesen werden

Nachdem die Datei eingelesen wurde, kann mithilfe der Methode
*genuegend_dreicke_vorhanden()* getestet werden, ob genauso viele
freie Flächen auf dem Quader vorhanden sind, wie Seiten keine
Dreiecke mitgegeben wurden.

Zusätzlich besitzt die Klasse eine *to_string()* Methode, die den
Titel der Datei und die Dimension ausgibt.

Die Klasse *Ausgabe* besteht aus folgenden Attributen:

- _titel       (String) : Titel der Ausgabedatei
- _dimension   (Tuple(int, int, int)) : Dimension des Puzzles
- _wuerfel     (Liste aus Würfeln) : Würfel mit den Lösungspositionen des Spielfeldes

Die Klasse *Ausgabe* wird mit dem Titel, der Dimension und den Würfeln
mit den Positionen auf dem Spielfeld erstellt und besitzt eine Methode
*create()* um die Ausgabedatei zu erstellen.

Sie erstellt die Ausgabedatei mit dem Namen, aus der Kommentarzeile
der eingelesenen Datei, iteriert anschließend über die vorhanden
Würfel und gibt diese in eine Datei aus.

Die erstellte Datei befindet sich anschließend im Ordner *output/*.

Klasse Würfel
+++++++++++++

Die Klasse *Würfel* besteht aus folgenden Attributen:

- _pos_x (int) : X - Position im Spielfeld
- _pos_y (int) : Y - Position im Spielfeld
- _pos_z (int) : Z - Position im Spielfeld
- _seiten (Liste aus Seiten) : Enthält alle 6 Seiten des Würfels
- _seite_1 (int) : Ausrichtung der 1. Seite des Würfels
- _seite_2 (int) : Ausrichtung der 2. Seite des Würfels
- _seite_3 (int) : Ausrichtung der 3. Seite des Würfels
- _seite_4 (int) : Ausrichtung der 4. Seite des Würfels
- _seite_5 (int) : Ausrichtung der 5. Seite des Würfels
- _seite_6 (int) : Ausrichtung der 6. Seite des Würfels
- _teil (String): Teilnummer des Würfels
- _dreiecke (int) : Enthält die Anzahl der Dreiecke eines Würfels

Mit der Methode *zaehle_dreiecke()* wird beim Anlegen eines Würfels die
Anzahl der Dreiecke gezählt, um später den Würfel zu kategorisieren.

Die Methode *set_positionen(pos_x, pos_y, pos_z)* ordnet dem Würfel die
Position in dem Spielfeld nach dem Lösen des Algorithmus zu.

Der Würfel besitzt die Methoden um sich um die X-Achse, Y-Achse und
Z-Achse drehen zu können.
Diese Methoden werden mit *plus* oder *minus* markiert, um
anzuzeigen, in welche Richtung der Würfel um 90° um die Achse gedreht wurde.
Es ist nur notwendig in einer Richtung einer Achse zu rotieren, um alle 24 Rotationen eines Würfels
vollständig abzudecken.

In der Methode *get_rotationen()* werden alle Rotationen eines Würfels
nacheinander ausgegeben.

Klasse Puzzle
+++++++++++++

Die Klasse *Puzzle* besteht aus folgenden Attributen:

- _wuerfel (Liste aus Würfeln) : Enthält Würfel, die zu Anfang übergeben werden
- _loesung (Liste aus Würfeln) : Enthält die gedrehten Würfel mit den Positionen im Spielfeld
- _dimension (Tuple(int, int int)) : Dimension des Puzzles
- _rank
- _gitter

- _ecken (Liste aus Eckwürfeln)
- _kanten (Liste aus Kantenwürfeln)
- _innen_flaeche (Liste aus Flächenwürfeln)
- _innen_raum (Liste aus Innenwürfeln)

Das Puzzle besteht aus dem Spielfeld in dem die Würfel
platziert werden.
Nach dem Erstellen des Puzzles werden die Würfel in die Kategorien
Eck -, Kanten -, Flächen - und Innenwürfel eingeteilt.
Das Puzzle besitzt die statische Methode *loese_puzzle()*.
Hier entsteht die Lösung des Algorithmus. Durch Setzen der
Würfel wird dieser erst mit den Rändern und dann
mit seinen potenziellen Nachbarn verglichen.
Mit der Methode *wuerfel_passen()* wird zuerst überprüft, ob sich der
Würfel am Rand befindet oder ob dieser mit seinen hinteren (X-, Y-Achse)
oder unterem Nachbar(Z-Achse) zusammen stecken lässt.
Dafür wird die Methode *laesst_sich_zusammenstecken()* aufgerufen.
In dieser wird geprüft, ob die Dreicke sich zusammenstecken lassen.
