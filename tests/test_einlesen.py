"""
Testklasse zum Testen zum Einlesen
"""

from src.eingabe import Eingabe
from src.puzzle import Puzzle
from src.ausgabe import Ausgabe

def test_einlesen():
    print("TEST EINLESEN")
    eingabe = Eingabe("input/Beispiel 1-2.txt")
    
    titel       = eingabe.get_titel
    wuerfel     = eingabe.get_wuerfel
    dimension   = eingabe.get_dimension
    print("Puzzle wird eingelesen")

    puzzle = Puzzle(dimension, wuerfel)
    print("Test: ", puzzle)
    if puzzle is not None:
        print("TEST OK")
    else:
        print("FEHLGESCHLAGEN")

    puzzle._loesung = wuerfel
    if puzzle._loesung is not None:
        print(puzzle)
        print("TEST OK")
    else:("TEST FEHLGESCHLAGEN")

if __name__ == "__main__":
    test_einlesen()
