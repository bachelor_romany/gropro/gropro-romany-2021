"""
Testklasse für die Einsortierung der Würfeltypen
"""

from src.eingabe import Eingabe
from src.puzzle  import Puzzle


def teste_sortierung(input):
    """
    teste ob die kleineste Anzahl an Ecken ausgegeben wird
    """
    print("TEST SORTIERUNG")
    eingabe = Eingabe(input)

    dim     = eingabe.get_dimension
    titel   = eingabe.get_titel
    wuerfel = eingabe.get_wuerfel
    print(titel)
    print(dim)
    puzzle = Puzzle(dim, wuerfel)

    ecken       = puzzle._ecken
    kanten      = puzzle._kanten
    flaechen    = puzzle._innen_flaeche
    innen_raum  = puzzle._innen_raum

    print("Ecken")
    for w in ecken:
        print(w)
    print("Kanten")
    for w in kanten:
        print(w)
    print("Flaechen")
    for w in flaechen:
        print(w)
    print("Innen")
    for w in innen_raum:
        print(w)

    if len(wuerfel) == len(ecken)+len(kanten)+len(flaechen)+len(innen_raum):
        print("TEST OK")
    else:
        print("FEHLGESCHLAGEN")

if __name__ == "__main__":
    teste_sortierung("input/Beispiel 1-1.txt")
    teste_sortierung("input/Beispiel 3.txt")
    teste_sortierung("input/Beispiel 4.txt")
    teste_sortierung("input/Beispiel 5.txt")
    teste_sortierung("input/Beispiel 6.txt")
    teste_sortierung("input/Beispiel 7.txt")

