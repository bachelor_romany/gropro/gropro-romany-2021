"""
Testklasse zum Drehen eines Wuerfels
"""

from src.wuerfel import Wuerfel

def test_X_drehen():
    print("TEST X- Drehung")

    wuerfel_1 = Wuerfel("1.", [0, 2, 4, 0, 0, 3])
    wuerfel_2 = Wuerfel("2.", [0, 0, 3, 2, 0, 1])

    print(wuerfel_1)
    print("drehe 1. Würfel")
    wuerfel_1.drehe_X_minus()
    print(wuerfel_1)
    print(wuerfel_2)

    if wuerfel_1.get_seiten == wuerfel_2.get_seiten:
        print("TEST OK")
    else:
        print("TEST FEHLGESCHLAGEN")

def test_Y_drehen():
    print("TEST Y- Drehung")

    wuerfel_1 = Wuerfel("1.", [2, 0, 4, 1, 0, 0])
    wuerfel_2 = Wuerfel("2.", [0, 0, 2, 4, 0, 4])

    print(wuerfel_1)
    print("drehe 1. Würfel")
    wuerfel_1.drehe_Y_minus()
    print(wuerfel_1)
    print(wuerfel_2)

    if wuerfel_1.get_seiten == wuerfel_2.get_seiten:
        print("TEST OK")
    else:
        print("TEST FEHLGESCHLAGEN")

def test_Z_drehen():
    print("TEST Z+ Drehung")

    wuerfel_1 = Wuerfel("1.", [3, 4, 1, 0, 0, 0])
    wuerfel_2 = Wuerfel("2.", [2, 0, 4, 1, 0, 0])

    print(wuerfel_1)
    print("drehe 1. Würfel")
    wuerfel_1.drehe_Z_plus()
    print(wuerfel_1)
    print(wuerfel_2)


    if wuerfel_1.get_seiten == wuerfel_2.get_seiten:
        print("TEST OK")
    else:
        print("TEST FEHLGESCHLAGEN")

if __name__ == "__main__":
    test_X_drehen()
    test_Y_drehen()
    test_Z_drehen()
