"""
Testklasse um die Eingabe und Ausgabe zu testen
"""

from src.eingabe import Eingabe
from src.ausgabe import Ausgabe

def test_beispiel_1():
    print("TEST EINGABE")
    eingabe = Eingabe("input/Beispiel 1-1.txt")
    
    wuerfel = eingabe.get_wuerfel
    titel = eingabe.get_titel
    dimension = eingabe.get_dimension

    print(eingabe)
    print("TEST OK")
    print("TEST DREIECKE")
    if eingabe.genuegend_dreiecke_vorhanden():
        print("Dreiecke wurden überprüft und der Algorithmus kann beginnen")
    
    # zur besseren Lesbarkeit, eine Konsolen Ausgabe
    # der bisherigen Würfel
    for w in wuerfel:
        print(w)
    # für den Test, die Ausgabe in eine Datei
    ausgabe = Ausgabe(titel, dimension, wuerfel)
    print("TEST AUSGABE")
    ausgabe.create()
    print("TEST OK")

if __name__ == "__main__":
    test_beispiel_1()
