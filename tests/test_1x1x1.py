"""
Testklasse für die Loesung eines 1x1x1 Puzzles
"""

from src.eingabe import Eingabe
from src.puzzle  import Puzzle

def teste_1x1x1():
    """
    Spezialfall, der nicht geloest werden kann
    """
    print("TEST 1x1x1")
    puzzle = Puzzle((1,1,1), [0,0,0,0,0,0])
    if puzzle is not None:
        print("TEST OK")
    else:
        print("TEST FEHLGESCHALGEN")

if __name__ == "__main__":
    teste_1x1x1()
