"""
Testklasse für die Loesung eines Puzzles
"""

from src.eingabe import Eingabe
from src.puzzle  import Puzzle
from src.ausgabe import Ausgabe


def teste_loesen(input):
    """
    """

    eingabe = Eingabe(input)

    dim     = eingabe.get_dimension
    titel   = eingabe.get_titel
    wuerfel = eingabe.get_wuerfel
    puzzle = Puzzle(dim, wuerfel)

    loesung = Puzzle.loese_puzzle(
            0,
            puzzle.get_sortierte_wuerfel,
            puzzle.create_game())
    
    puzzle.set_loesung(loesung)


    ausgabe = Ausgabe(titel, dim, puzzle.get_loesung)
    
    ausgabe.create()
    print(titel)
    if ausgabe is not None:
        print("TEST OK")
        return True
    else:
        print("TEST FEHLGESCHLAGEN")
        return False

if __name__ == "__main__":
    teste_loesen("input/Beispiel 1-1.txt")
    teste_loesen("input/Beispiel 2-1.txt")
    teste_loesen("input/Beispiel 3.txt")
    teste_loesen("input/Beispiel 4.txt")
    teste_loesen("input/Beispiel 5.txt")
    print("Das Lösen des Würfels mit 3x3x3 dauert ca 90 Sek")
    # teste_loesen("input/Beispiel 7.txt")
