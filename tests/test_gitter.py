"""
Testklasse zum Erzeugen von Gitterstrukturen
"""

from src.puzzle import Puzzle
from src.wuerfel import Wuerfel

def teste_gitter():
    print("TESTE GITTER")
    test_dimension = (1,2,1)

    wuerfel_1 = Wuerfel("Teil 1:", [0,0,1,0,0,0])
    wuerfel_2 = Wuerfel("Teil 2:", [0,0,0,4,0,0])
    neues_puzzle = Puzzle(test_dimension, [wuerfel_1, wuerfel_2])

    # erstelle Gitterstruktur
    gitter = neues_puzzle.create_game()
    if gitter is not None:
        print("TEST OK")
    else:
        print("TEST FEHLGESCHLAGEN")

if __name__ == "__main__":
    teste_gitter()
