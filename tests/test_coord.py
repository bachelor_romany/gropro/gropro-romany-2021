"""
Testklasse zum Testen der Koordinaten
"""

from src.puzzle import Puzzle

def teste_koordinaten():
    print("TESTE KOORDINATEN")
    count   = 0
    groesse = (2,2,2)
    dim     = groesse[0]*groesse[1]*groesse[2]
    for pos in range(dim):
        count += 1
        print(Puzzle.get_coord(pos, groesse))
    
    if count == dim:
        print("TEST OK")
    else:
        print("TEST FEHLGESCHLAGEN")


if __name__ == "__main__":
    teste_koordinaten()
