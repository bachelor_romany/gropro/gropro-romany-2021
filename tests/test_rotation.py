from src.wuerfel import Wuerfel

def test_rotation():
    print("TEST ROTATIONEN")
    wuerfel = Wuerfel("1: ", [0,0,4,0,0,0])
    count = 0
    for rotationen in wuerfel.get_rotationen():
        count += sum(rotationen.get_seiten)
        print(rotationen)

    if count == 60:
        print("TEST OK")
    else:
        print("TEST FEHLGESCHLAGEN")
    
def _rotation():

    x = Wuerfel("1. ", [1,1,1,1,1,1])
    y = Wuerfel("1. ", [1,1,1,1,1,1])
    z = Wuerfel("1. ", [1,1,1,1,1,1])

    print(x)
    x.drehe_X_minus()
    print(x)
    y.drehe_Y_minus()
    print(y)
    z.drehe_Z_plus()
    print(z)
if __name__ == "__main__":
    test_rotation()

    # _rotation()
