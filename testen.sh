#!/bin/bash

test_folder="./tests/"
fehler="./input/fehler"
sonder="./input/sonder"

FILES=($(/bin/ls $test_folder))

for file in ${FILES[@]}
do
    python3 $test_folder/$file
done

echo TEST der Fehlerfaelle

FILES=($(/bin/ls $fehler))

for file in ${FILES[@]}
do
    python3 main.py $fehler/$file
done

FILES=($(/bin/ls $sonder))
echo TESTE den Sonderfall, mit einem Dreieck auf der Oberfläche

for file in ${FILES[@]}
do
    python3 main.py $sonder/$file
done
