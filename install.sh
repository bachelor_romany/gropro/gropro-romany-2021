#!/bin/bash


echo Erzeuge virtuelle Umgebung

env_name="holzpuzzle"

if [ $# -gt 0 ]; then
    env_name=$1
fi

if [ -d $env_name ]; then
    echo Das Verzeichnis existiert bereits
    echo Lösche Umgebung
    rm $env_name
fi
echo Installiere Umgebung

python3 -m venv $env_name

echo Aktualisiere Umgebung

source $env_name/bin/activate

echo Upgrade PIP

pip3 install --upgrade pip

echo Installiere Umgebung in Root

pip3 install -e .

# ueberpruefe ob direnv installiert ist, und legt eine .envr an
command direnv --version > /dev/null 2>&1

# falls direnv installiert ist, wird eine .envrc Datei generiert
if [ $? == 0 ]; then

    echo generating envronment file for direnv
    echo layout python ${env_name} > .envrc
    direnv allow
    echo Done
fi

echo Umgebung wurde installiert
