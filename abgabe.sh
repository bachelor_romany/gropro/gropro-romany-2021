#! /bin/bash

cp docs/build/latex/Dokumentation.pdf .
zip romany_roman_20630.zip Dokumentation.pdf
zip romany_roman_20630.zip install.sh
zip romany_roman_20630.zip main.py
zip romany_roman_20630.zip README.md
zip romany_roman_20630.zip setup.py

zip -r romany_roman_20630.zip input/
zip -r romany_roman_20630.zip output/
zip -r romany_roman_20630.zip src/*.py

zip romany_roman_20630.zip testen.sh
zip -r romany_roman_20630.zip tests/
