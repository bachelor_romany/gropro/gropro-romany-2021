"""
Klasse Ausgabe
"""
import os
from typing import Tuple, List

from src.wuerfel import Wuerfel
from src.utils   import get_project_root

class Ausgabe():
    """
    Die Klasse erzeugt eine Ausgabedatei.

    Args:
        titel (str): Titel der Datei
        dimension (tuple[int, int, int]): Größe des Spielfeldes
        wuerfel (str): Würfel mit Positionen nachdem der Algorithmus angewendet wurde
    """
    def __init__(self, titel: str, dimension: Tuple[int, int, int], wuerfel: List[Wuerfel]):

        self._titel     = titel
        self._dimension = dimension
        self._wuerfel   = wuerfel


    def create(self) -> None:
        """
        Erzeugt eine Ausgabe in der Konsole und in eine Datei

        Returns:
            None
        """
        print(self._titel)
        dateiname = self._titel.split("\n")[1].replace("/", " ").strip()
        dimension_text = "Dimension {},{},{}\n".format(self._dimension[0], self._dimension[1], self._dimension[2])
        print(dimension_text)
        with open(os.path.join(get_project_root(), "output/", dateiname + ".out"), "w") as f:
            f.write(self._titel)
            f.write(dimension_text)
            for wuerfel in self._wuerfel:
                print(wuerfel)
                f.write(wuerfel.__str__() + "\n")
