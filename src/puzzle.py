"""
Klasse Puzzle
"""
import numpy as np
from copy import deepcopy
from typing import List, Tuple

from src.wuerfel import Wuerfel

class Puzzle():
    """
    Die Klasse erzeugt ein Spielfeld für ein Holzpuzzle

    Es besteht aus den Würfeln, die der Eingabe entnommen wurde und dem eigentlichen Algorithmus.
    Zuerst wird der Rang des Systems bestimmt und ein Spielfeld/Gitter angelegt.
    Danach werden die Würfel in verschiedene Typen kategorisiert,
    damit der Algorithmus an den jeweiligen Koordinaten des Gitters einen Würfeltyp ansetzen kann
    und nicht jeder Würfel an jeder Stelle benutzt werden muss.

    Args:
        dimension (tuple[int, int, int]): Dimension, aus der das Spielfeld/Gitter erzeugt wird
        wuerfel_liste (list()): Liste aus Würfeln, die bei der Eingabe eingelesen wurden

    Attributes:
        _loesung (list[Wuerfel]): Liste aus gelösten Würfel, wenn der Algorithmus durchläuft, sonst None
        _rank (int): Rang des Puzzles
        _gitter (np.ndarray): Dreidimensionale Matrix, welches die Würfel auf die Positionen speichert.
        _ecken (list[]): Liste aus Eckwürfeln
        _kanten (list[]): Liste aus Kantenwürfeln
        _innen_flaeche (list[]): Liste aus Flächenwürfeln
        _innen_raum (list[]): Liste aus Innenwürfeln
        _sortierte_wuerfel (tuple[list[]]): Behinhaltet alle Typen von Würfeln
    """

    def __init__(self, dimension: Tuple[int, int, int], wuerfel: List[Wuerfel]):

        self._wuerfel   = wuerfel
        self._loesung   = list()
        self._dimension = dimension
        self._rank      = self.get_rank()
        self._gitter    = self.create_game()

        self._ecken         = list()
        self._kanten        = list()
        self._innen_flaeche = list()
        self._innen_raum    = list()

        
        # Sortiert die einzelnen Würfel in die oberen Listen
        # um die Komplexität des Algorithmus zu verkleinern
        self.sortiere_wuerfel_ein(self._rank)
        self._sortierte_wuerfel = (
                self._ecken,
                self._kanten,
                self._innen_flaeche,
                self._innen_raum)


    @property
    def get_sortierte_wuerfel(self):
        """
        Getter Methode für die kategorisierten Würfel

        Returns:
            _sortierte_wuerfel (tuple[list[]])
        """
        return self._sortierte_wuerfel

    @property
    def get_loesung(self):
        """
        Getter Methode für die gelösten Würfel, um dieser der Ausgabe zur Verfügung zu stellen

        Returns:
            _loesung (list[Wuerfel])
        """
        return self._loesung

    def set_loesung(self, gitter):
        """
        Setter Methode zum Setzen der Koordinaten

        Nach dem Loesen des Puzzles müssen die Positionen den Würfeln zugeteilt werden

        Args:
            gitter(np.ndarray): Spielfeld mit gesetzten Würfeln und deren Ausrichtung der Dreiecke
        """
        if gitter is not None:
            for x in range(gitter.shape[0]):
                for y in range(gitter.shape[1]):
                    for z in range(gitter.shape[2]):
                        gitter[x,y,z].set_position(x + 1,y + 1,z + 1)
                        self._loesung.append(gitter[x,y,z])
            # ueberschreibe altes Gitter mit neuem geloesten Gitter
            self._gitter = gitter
        else:
            print("Es gibt keine Lösung")



    def create_game(self) -> np.ndarray:
        """
        Erstellt das Gitter, welches zum Lösen des Puzzles benötigt wird

        Returns:
            numpy array in 3 Dimensionen, gefüllt mir leeren Würfeln
        """
        gitter = np.empty((
            self._dimension[0],
            self._dimension[1],
            self._dimension[2]), dtype=Wuerfel)
        return gitter

    @staticmethod
    def loese_puzzle(pos, freie_wuerfel: Tuple[List[Wuerfel]], spielfeld: np.ndarray) -> np.ndarray:
        """
        Backtracking Algorithmus

        Hier wird loese_puzzle() rekursiv auf die nächste Position aufgerufen
        Ein passender Würfel wird auf die Position aus den noch freien Würfeln gezogen
        und es wird überprüft ob der Würfel hier hin gesetzt werden kann
        Ist dem nicht der Fall, werden alle Rotationen der Reihe nach durchprobiert
        Wenn dieser Würfel immer noch nicht passt, wird ein Schritt zurück gegangen
        und ein anderer Würfel des gleichen Typs wird überprüft
        Dies wird so lange fortgeführt bis keine Würfel mehr vorhanden sind
        oder alle Würfel ausprobiert wurden

        Ars:
            pos(int): Position des zu setzenden Würfels in dem Gitter
            freie_wuerfel(tuple[list[Wuerfel]]): die noch freien Würfel die vorhanden sind, kategorisiert nach den Würfeltypen
            spielfeld(np.ndarray): Spielfeld, oder Gitter, welches vorher generiert wurde, in dem nun die Würfel platziert werden

        Returns:
            spielfeld(np.ndarray): Es werden die Würfel mit Rotationen an den Positionen gesetzt ausgegeben, 
            falls keine Lösung gefunden wird, wird None zurückgegeben
        """
        if len(freie_wuerfel[0]) == 0:
            return spielfeld
        # rechnet Position in Koordinate um
        koordinate = Puzzle.get_coord(pos, spielfeld.shape)
        # ermittle Würfeltyp für die Koordinate
        wuerfeltyp = Puzzle.get_richtiger_wuerfeltyp(koordinate, spielfeld.shape)
        # iteriere über alle Würfel, die an diese Stelle platziert werden können
        for wuerfel in freie_wuerfel[wuerfeltyp]:
            # rotiere Wuerfel
            for rotierter_wuerfel in wuerfel.get_rotationen():
                # passt der Wuerfel in das System
                # falls ja, setzt ihn und fuehrt rekursiv fort
                # wenn nicht, rotiere den rotierten Würfel eine Stufe weiter
                if Puzzle.wuerfel_passen(rotierter_wuerfel, koordinate, spielfeld):
                    spielfeld[koordinate] = rotierter_wuerfel
                    kopie = deepcopy(freie_wuerfel)
                    kopie[wuerfeltyp].remove(wuerfel)
                    geloest = Puzzle.loese_puzzle(pos+1, kopie, spielfeld)
                    if geloest is not None:
                        return geloest

    @staticmethod
    def get_coord(pos: int, shape: Tuple[int, int, int]) -> Tuple[int, int, int]:
        """
        Rechnet die Position in einen 3D Index um

        Returns:
            tuple[int, int, int]: Gibt die Koordinaten der Position aus von einer gegebenen Groesse
        """
        z = pos//(shape[0]*shape[1])
        yx = pos%(shape[0]*shape[1])
        y = yx//shape[0]
        x = yx%shape[0]

        return (x,y,z)
    
    @staticmethod
    def get_richtiger_wuerfeltyp(koordinate: Tuple[int, int, int], shape: Tuple[int, int, int]) -> int:
        """
        Gibt den passenden Index fuer das sortierte Würfeltuple aus

        Berechnet anhand der Ränder, an welcher Position sich die Koordinate befindet.

        Args:
            koorinate(tuple[int, int, int]): Koordinate im Gitter
            shape(tuple[int, int, int]): Umriss des gitters

        Returns:
            int
        """
        anzahl_raender = 0
        for i in range(3):
            if koordinate[i] == shape[i] - 1 or koordinate[i] == 0:
                anzahl_raender += 1
        return (3 - anzahl_raender)

    @staticmethod
    def laesst_sich_zusammenstecken(wuerfel: Wuerfel, koordinate: Tuple[int], spielfeld: np.ndarray, richtung: str):
        """
        Prüft die X, Y und Z Nachbarn, ob die Dreiecke sich zusammenstecken lassen

        Args:
            wuerfel(Wuerfel): Würfel an der Position im Spielfeld
            koordinate(tuple[int, int, int]): Koordinatenpunkt im Spielfeld
            spielfeld(np.ndarray): spielfeld mit Würfeln aus aktuellem Spielstand
            richtung(str): Welcher Nachbar geprüft werden soll aus 'x', 'y' oder 'z' Richtung

        Returns:
            True, falls sich die Dreiecke zusammenstecken lassen, sonst False
        """
        if richtung == "x":
            if wuerfel.get_seiten[4] == 3 and spielfeld[koordinate[0]-1, koordinate[1], koordinate[2]].get_seiten[2] != 4 or \
                    wuerfel.get_seiten[4] == 4 and spielfeld[koordinate[0]-1, koordinate[1], koordinate[2]].get_seiten[2] != 3 or\
                    wuerfel.get_seiten[4] == 1 and spielfeld[koordinate[0]-1, koordinate[1], koordinate[2]].get_seiten[2] != 2 or \
                    wuerfel.get_seiten[4] == 2 and spielfeld[koordinate[0]-1, koordinate[1], koordinate[2]].get_seiten[2] != 1:
                return False
        if richtung == "y":
            if wuerfel.get_seiten[1] == 3 and spielfeld[koordinate[0], koordinate[1] - 1, koordinate[2]].get_seiten[3] != 4 or \
                    wuerfel.get_seiten[1] == 4 and spielfeld[koordinate[0], koordinate[1] - 1, koordinate[2]].get_seiten[3] != 3 or\
                    wuerfel.get_seiten[1] == 1 and spielfeld[koordinate[0], koordinate[1] - 1, koordinate[2]].get_seiten[3] != 2 or \
                    wuerfel.get_seiten[1] == 2 and spielfeld[koordinate[0], koordinate[1] - 1, koordinate[2]].get_seiten[3] != 1:
                return False
        if richtung == "z":
            if wuerfel.get_seiten[5] == 3 and spielfeld[koordinate[0], koordinate[1], koordinate[2] - 1].get_seiten[0] != 2 or \
                    wuerfel.get_seiten[5] == 2 and spielfeld[koordinate[0], koordinate[1], koordinate[2] - 1].get_seiten[0] != 3 or\
                    wuerfel.get_seiten[5] == 1 and spielfeld[koordinate[0], koordinate[1], koordinate[2] - 1].get_seiten[0] != 4 or \
                    wuerfel.get_seiten[5] == 4 and spielfeld[koordinate[0], koordinate[1], koordinate[2] - 1].get_seiten[0] != 1:
                return False


        return True
        


    @staticmethod
    def wuerfel_passen(wuerfel: Wuerfel, koordinate: Tuple[int, int, int], spielfeld: np.ndarray) -> bool:
        """
        Prüft ob der Würfel in die Stelle eingefügt werden kann

        Zuerst wird überprüft ob in negativer X, Y oder Z Richtung der Rand des Puzzles ist.
        Wenn der Würfel am Rand ist, muss geprüft werden, dass kein Dreieck zu dieser Seite hin zeigt.
        Falls sich 'hinter' dem Würfel weitere Würfel befinden, wird die Methode
        'laesst_sich_zusammenstecken()' aufrufen.

        Wenn der Würfel in positiver X, Y oder Z Richtung auf dem Rand liegt, dürfen in dieser Richtung
        keine Dreiecke nach außen zeigen.
        
        Args:
            wuerfel (Wuerfel): Würfel, der an die Stelle platziert werden soll
            koorinate (tuple[int, int, int]): Die Koordinate an der der Würfel platziert werden soll
            spielfeld (numpy.ndarray): Das Gitter, in dem die Würfel platziert werden.

        Returns:
            True, wenn der Würfel dort platziert werden kann, sonst False
        """
        # X Nachbar
        if koordinate[0] == 0:
            if wuerfel.get_seiten[4] != 0:
                return False
        elif not Puzzle.laesst_sich_zusammenstecken(wuerfel, koordinate, spielfeld, "x"):
            return False
        # Y Nachbar
        if koordinate[1] == 0:
            if wuerfel.get_seiten[1] != 0:
                return False
        elif not Puzzle.laesst_sich_zusammenstecken(wuerfel, koordinate, spielfeld, "y"):
                return False
        # Z Nachbar
        if koordinate[2] == 0:
            if wuerfel.get_seiten[5] != 0:
                return False
        elif not Puzzle.laesst_sich_zusammenstecken(wuerfel, koordinate, spielfeld, "z"):
                return False
        # Randbedingungen ueberpruefen
        if koordinate[0] == spielfeld.shape[0] - 1:
            if wuerfel.get_seiten[2] != 0:
                return False
        if koordinate[1] == spielfeld.shape[1] - 1:
            if wuerfel.get_seiten[3] != 0:
                return False
        if koordinate[2] == spielfeld.shape[2] - 1:
            if wuerfel.get_seiten[0] != 0:
                return False

        return True

    def get_rank(self) -> int:
        """
        Gibt die Dimension(Rang) des Systems zurück

        Returns:
            rank(int): Rang des Puzzles
        """
        rank = 0
        for dim in self._dimension:
            if dim != 1:
                rank += 1
        
        return rank

    def sortiere_wuerfel_ein(self, dim: int) -> None:
        """
        Sortiert in Abhängigkeit des gegebenen Rangs die Würfel
        nach Ecken, Kanten, Flächen und Innenstücken
        
        Args:
            dim(int): Rang des Puzzles
        
        Returns:
            None
        """
        if dim == 0:
            print("Spezialfall 1x1x1, es existiert nur ein Würfel, Lösung ist trivial.")
            return exit(0)
        elif dim == 1:
            for wuerfel in self._wuerfel:
                if wuerfel.get_dreiecke == 1:
                    self._ecken.append(wuerfel)
                else:
                    self._kanten.append(wuerfel)
        elif dim == 2:
            for wuerfel in self._wuerfel:
                if wuerfel.get_dreiecke == 2:
                    self._ecken.append(wuerfel)
                elif wuerfel.get_dreiecke == 3:
                    self._kanten.append(wuerfel)
                else:
                    self._innen_flaeche.append(wuerfel)
        elif dim == 3:
            for wuerfel in self._wuerfel:
                if wuerfel.get_dreiecke == 3:
                    self._ecken.append(wuerfel)
                elif wuerfel.get_dreiecke == 4:
                    self._kanten.append(wuerfel)
                elif wuerfel.get_dreiecke == 5:
                    self._innen_flaeche.append(wuerfel)
                else:
                    self._innen_raum.append(wuerfel)
        else:
            print("Algorithmus nicht für mehr als 3 Dimensionen ausgelegt")
        

        

    def __str__(self) -> str:
        """
        to_string Methode vom Puzzle

        Gibt die Loesungswürfel aus

        Returns:
            text (str)
        """
        text = ""
        if len(self._loesung) == 0:
            text += "Es gibt keine Loesungen"
        else:
            for wuerfel in self._loesung:
                print(wuerfel)
        print(self._gitter)

        return text
