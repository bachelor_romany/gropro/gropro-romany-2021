"""
Hilfsmethoden für das erstellte Environment
"""

from pathlib import Path

def get_project_root() -> Path:
    """
    Gibt das root Verzeichnis des Projekts aus

    Returns:
        Pfad des 'Root' Verzeichnis
    """
    return Path(__file__).parent.parent
