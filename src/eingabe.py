"""
Klasse Eingabe
"""
import os
from typing import List, Tuple

from src.wuerfel import Wuerfel
from src.utils import get_project_root


class Eingabe():
    """
    Die Klasse erzeugt ein Grundgerüst um eine Struktur der eingelesenen Daten zu erzeugen.

    Args:
        input (str): Name der Eingabedatei

    Attributes:
        _dimension (tuple[int, int, int]): Gibt die Dimension des Puzzles wieder
        _wuerfel_liste (list()): Liste aus Würfeln
        _titel (str): Titel der Eingabedatei
        _oberflaeche (int): Oberfläche des Quaders
        _leere_flaechen (int): Leere Flächen die von den Würfeln ausgelesen werden
    """
    def __init__(self, input: str):
        
        self._dimension         = Tuple[int, int, int]
        self._wuerfel_liste     = list()
        self._titel             = ""
        self._input             = input
        self._oberflaeche       = 0
        self._leere_flaechen    = 0

        self.lese_datei()


    @property
    def get_wuerfel(self) -> List[Wuerfel]:
        """
        Getter Methode für die eingelesenen Würfel

        Returns:
            list[Wuerfel]
        """
        return self._wuerfel_liste

    @property
    def get_dimension(self) -> Tuple[int, int, int]:
        """
        Getter Methode für die Dimension
        
        Returns:
            tuple[int, int, int]
        """
        return self._dimension

    @property
    def get_titel(self) -> str:
        """
        Getter Methode für den Titel

        Returns:
            str
        """
        return self._titel

    def lese_datei(self) -> None:
        """
        Liest die Dateien aus der eingegebenen Datei
        
        Returns:
            None
        """
        zeilennummer = 0
        with open(os.path.join(get_project_root(), self._input), "r") as datei:
            for zeile in datei:
                # erhöhe Zeilennummer um 1
                zeilennummer += 1
                if zeile.startswith("/"):
                    self._titel += zeile
                elif zeile.startswith("Dimension"):
                    dimension = zeile.split(" ")
                    dimension = dimension[1].split(",")
                    dim_x = int(dimension[0])
                    dim_y = int(dimension[1])
                    dim_z = int(dimension[2])
                    groesse = dim_x * dim_y * dim_z
                    # Dimension == 0?
                    if groesse == 0:
                        print("Zeile {} Fehler in der Dimension, Dimension 0?".format(zeilennummer))
                        exit(1)
                    # Dimension negativ?
                    if dim_x < 0 or dim_y < 0 or dim_z < 0:
                        print("Zeile {} Fehler in der Dimension, Dimension < 0?".format(zeilennummer))
                        exit(1)
                    self._dimension = int(dimension[0]), int(dimension[1]), int(dimension[2])
                else:
                    wuerfel = zeile.split()
                    teil = wuerfel[0] + " " + wuerfel[1]
                    seiten = []
                    for i in range(2,8):
                        seite = int(wuerfel[i])
                        if seite == 0:
                            self._leere_flaechen += 1
                        # Seite < 0
                        if seite < 0:
                            print("Zeile {} Fehler in {} Orientierung < 0?".format(zeilennummer, teil))
                            exit(1)
                        # Seite > 4
                        if seite > 4:
                            print("Zeile {} Fehler in {} Orientierung > 4?".format(zeilennummer, teil))
                            exit(1)
                        seiten.append(seite)
                    self._wuerfel_liste.append(Wuerfel(teil, seiten))
        # mehr Würfel als Dimensionen?
        if len(self._wuerfel_liste) > groesse:
            print("Zeile {} Fehler in {} mehr Würfel als Dimensionen?".format(zeilennummer, teil))
            exit(1)
        # weniger Würfel als Dimensionen?
        if len(self._wuerfel_liste) < groesse:
            print("Weniger Würfel als Dimensionen?".format(zeilennummer, teil))
            exit(1)
    
    def genuegend_dreiecke_vorhanden(self) -> bool:
        """
        Überprüft, ob zu viele oder zu wenige Dreiecke im Puzzle enthalten sind.

        Durch die Überprüfung kann nicht ermittelt werden, ob ein einzelnes Puzzleteil
        zu viele oder zu wenige Dreicke hat

        Returns:
            True, wenn genügend Dreicke vorhanden sind, sonst False
        """
        a = self._dimension[0]
        b = self._dimension[1]
        c = self._dimension[2]

        self._oberflaeche = 2*a*b + 2*a*c + 2*b*c

        return self._oberflaeche == self._leere_flaechen
    
    def __str__(self):
        """
        to_string Methode in Python

        Returns:
            text (str): Stellt die Klasse dar
        """
        text = ""
        text += self._titel
        text += "Dimension "
        text += "{},{},{}".format(
                str(self._dimension[0]),
                str(self._dimension[1]),
                str(self._dimension[2]))

        return text
