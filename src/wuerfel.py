"""
Klasse Würfel
"""
from copy import deepcopy
from typing import List

class Wuerfel():
    """
    Die Klasse erzeugt einen Würfel

    Diesem Würfel wird nach dem Anlegen die Seiten zugeordnet und die Anzahl an Dreiecke wird gezählt.

    Args:
        teil (str): Name des Würfels
        seiten (list[int]): Liste aus Seiten, mit Orientierung der Dreiecke

    Attributes:
        _pos_x (int): X Position im Spielfeld
        _pos_y (int): Y Position im Spielfeld
        _pos_z (int): Z Position im Spielfeld
        _seite_1 (int): Ausrichtung der 1. Seite
        _seite_2 (int): Ausrichtung der 2. Seite
        _seite_3 (int): Ausrichtung der 3. Seite
        _seite_4 (int): Ausrichtung der 4. Seite
        _seite_5 (int): Ausrichtung der 5. Seite
        _seite_6 (int): Ausrichtung der 6. Seite
        _dreiecke (int): Anzahl der Dreieckedes Würfels
    """

    def __init__(self, teil: str, seiten: List[int]):
        self._pos_x = 0
        self._pos_y = 0
        self._pos_z = 0
        self._seiten = seiten
        self._seite_1 = seiten[0]
        self._seite_2 = seiten[1]
        self._seite_3 = seiten[2]
        self._seite_4 = seiten[3]
        self._seite_5 = seiten[4]
        self._seite_6 = seiten[5]
        
        self._teil = teil
        self._dreiecke = 0
        self.zaehle_dreiecke()

    @property
    def get_seiten(self) -> list():
        """
        Getter für die Seiten des Würfels
        
        Returns:
            seiten(list[])
        """
        return self._seiten

    @property
    def get_dreiecke(self) -> int:
        """
        Getter Methode für Dreiecke auf dem Würfel

        Returns:
            dreicke(int)
        """
        return self._dreiecke

    def set_position(self, x: int, y: int, z: int) -> None:
        """
        Setze die Positionen des Würfels

        Args:
            x(int): X Position
            y(int): Y Position
            z(int): Z Position
        """
        self._pos_x = x
        self._pos_y = y
        self._pos_z = z

    def drehe_X_minus(self) -> None:
        """
        Dreht den Würfel um -90° um die X Achse

        Returns:
            None
        """
        # Seite 3 dreht sich um die eigene Achse
        if self._seite_3 != 0:
            self._seite_3 -= 1
            if self._seite_3 == 0:
                self._seite_3 = 4
        # Seite 5 dreht sich um die eigene Achse
        if self._seite_5 != 0:
            self._seite_5 += 1
            if self._seite_5 > 4:
                self._seite_5 = 1
        # speichere Seite 2 zwischen
        tmp = self._seite_2
        # drehe Seite 1 auf Seite 2
        if self._seite_1 != 0:
            self._seite_2 = self._seite_1 - 1
            if self._seite_2 == 0:
                self._seite_2 = 4
        else:
            self._seite_2 = 0
        # drehe Seite 4 auf Seite 1
        if self._seite_4 != 0:
            self._seite_1 = self._seite_4 - 1
            if self._seite_1 == 0:
                self._seite_1 = 4
        else:
            self._seite_1 = 0
        # drehe Seite 6 auf Seite 4
        if self._seite_6 != 0:
            self._seite_4 = self._seite_6 - 1
            if self._seite_4 == 0:
                self._seite_4 = 4
        else:
            self._seite_4 = 0
        # drehe zuletzt Seite 2 auf Seite 6
        if tmp != 0:
            self._seite_6 = tmp - 1
            if self._seite_6 == 0:
                self._seite_6 = 4
        else:
            self._seite_6 = 0
        # setze die Seiten neu
        self._seiten = [self._seite_1, self._seite_2, self._seite_3, self._seite_4, self._seite_5, self._seite_6]

    def drehe_Y_minus(self) -> None:
        """
        Dreht den Würfel um -90° um die Y Achse

        Returns:
            None
        """
        # Seite 2 dreht sich um die eigene Achse
        if self._seite_2 != 0:
            self._seite_2 += 1
            if self._seite_2 > 4:
                self._seite_2 = 1
        # Seite 4 dreht sich um die eigene Achse
        if self._seite_4 != 0:
            self._seite_4 -= 1
            if self._seite_4 == 0:
                self._seite_4 = 4
        # speichere Seite 5 zwischen
        tmp = self._seite_5
        # drehe Seite 3 auf Seite 6
        self._seite_6 = self._seite_3
        # drehe Seite 1 auf Seite 3
        self._seite_3 = self._seite_1
        # drehe Seite 6 auf Seite 5
        if self._seiten[5] != 0:
            # drehe Dreieck um 180°
            if self._seiten[5] > 2:
                self._seite_5 = self._seiten[5] - 2
            else:
                self._seite_5 = self._seiten[5] + 2
        else:
            self._seite_5 = 0
        # zuletzt drehe Seite 5 auf Seite 1
        if tmp != 0:
            # drehe Dreieck um 180°
            if self._seiten[4] > 2:
                self._seite_1 = self._seiten[4] - 2
            else:
                self._seite_1 = self._seiten[4] + 2
        else:
            self._seite_1 = 0
        # setze die Seiten neu
        self._seiten = [self._seite_1, self._seite_2, self._seite_3, self._seite_4, self._seite_5, self._seite_6]

    def drehe_Z_plus(self) -> None:
        """
        Dreht den Würfel um 90° um die Z Achse

        Returns:
            None
        """
        # Seite 1 dreht sich um die eigene Achse
        if self._seite_1 != 0:
            self._seite_1 -= 1
            if self._seite_1 == 0:
                self._seite_1 = 4
        # Seite 6 dreht sich um die eigene Achse
        if self._seite_6 != 0:
            self._seite_6 += 1
            if self._seite_6 > 4:
                self._seite_6 = 1
        # Rotieren der anderen Seiten
        self._seite_2, self._seite_3, self._seite_4, self._seite_5 \
                = self._seite_5, self._seite_2, self._seite_3, self._seite_4

        # setze die Seiten neu
        self._seiten = [
                self._seite_1, self._seite_2, self._seite_3,
                self._seite_4, self._seite_5, self._seite_6]

    def zaehle_dreiecke(self) -> None:
        """
        Zählt die Anzahl der Dreiecke auf einem Würfel

        Iteriert durch die Seiten des Würfels und zählt bei einem Dreieck
        die Variable 'dreiecke' um 1 hoch

        Returns:
            None
        """

        for dreieck in self._seiten:
            if dreieck != 0:
                self._dreiecke += 1

        return

    
    def get_rotationen(self):
        """
        Generatorfunktion, die die Drehung eines aktuellen Würfels ausgibt

        Dabei wird wie aus einer Liste der nächste Wert ausgegeben.
        Diese Liste wird aber nicht gespeichert, sondern es wird stets die nächste
        Rotation ausgegeben, die an der nächsten Stelle steht.
        Maximal werden 24 Rotationen zurückgegeben.

        Yields:
            Wuerfel: dreht den Würfel nach einem bstimmten Muster und
            gibt so die nächste Rotation aus

        Examples:
            >>> print(for rotierter_wuerfel in wuerfel.get_rotationen())
            [rotiertel_wuerfel, rotierter_wuerfel.drehe_X_minus() ...]
        """
        kopie = deepcopy(self)
        yield kopie
        for _ in range(3):
            kopie.drehe_X_minus()
            yield kopie
        kopie.drehe_Z_plus()
        yield kopie
        for _ in range(3):
            kopie.drehe_Y_minus()
            yield kopie
        kopie.drehe_Z_plus()
        yield kopie
        for _ in range(3):
            kopie.drehe_X_minus()
            yield kopie
        kopie.drehe_Z_plus()
        yield kopie
        for _ in range(3):
            kopie.drehe_Y_minus()
            yield kopie
        kopie.drehe_X_minus()
        yield kopie
        for _ in range(3):
            kopie.drehe_Z_plus()
            yield kopie
        kopie.drehe_X_minus()
        kopie.drehe_X_minus()
        yield kopie
        for _ in range(3):
            kopie.drehe_Z_plus()
            yield kopie


    def __str__(self) -> str:
        """
        to_string Methode für einen Würfel

        Returns:
            text (str)
        """
        text = ""
        text += "[{},{},{}] ".format(self._pos_x, self._pos_y, self._pos_z)
        text += self._teil
        text += " {} {} {} {} {} {}".format(
                self._seite_1, self._seite_2, self._seite_3,
                self._seite_4, self._seite_5, self._seite_6)

        return text

    def __repr__(self):
        """
        repräsentiert den Würfel

        In Python notwendig, um fstrings zu erzeugen

        Returns:
            __str__()
        """
        return self.__str__()

    def __eq__(self, anderer_wuerfel) -> bool:
        """
        Methode um 2 Würfel zu vergleichen

        Da bei einer Deepcopy ein neues Objekt angelegt wird, kann man diese nicht mehr vergleichen,
        so wird im folgenden der Operater der die Gleichheit bestimmt überschreiben, und es wird
        geprüft ob der Teilname der beiden Würfel identisch ist
        
        Args:
            anderer_wuerfel (Wuerfel): Der Würfel der mit dem aktuellen Würfel verglichen wird
        
        Returns:
            True, wenn beide Würfel den gleichen Teilenamen haben, sonst False

        """
        return self._teil == anderer_wuerfel._teil
