
# Installationsanleitung

Das Programm läuft unter Linux

Mit der Datei 'install.sh' installiert das Programm eine Python Umgebung, die man anschließend aktivieren muss
Mit 'source holzpuzzle/bin/activate'

Mit der 'main.py' kann das Programm gestartet werden

```
python main.py datei
```

[Hier ist die Dokumentation](Dokumentation.pdf)
