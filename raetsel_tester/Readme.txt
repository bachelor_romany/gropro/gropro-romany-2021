****************************************************
*        Tools f�r die Pr�fung Sommer 2021         *
****************************************************

Die Tools in diesem Vereichnis dienen dazu
die in ihrem Programm erzeugten Ausgaben
auf Richtigkeit zu �berpr�fen.

WuerfelGenerator
Der Wuerfelgenerator erzeugt Puzzle mit den angegebenen Dimensionen
sowie eine dazugeh�rige Musterl�sung.
Der Aufruf erfolgt �ber

java -jar Wuerfelgenerator.jar <x> <y> <z> Bezeichnung

<x> <y> <z> - notwendige Koordinaten, m�ssen zwischen mindestens 1 und 10 sein.
	      Die Koordinaten sind durch Leerzeichen getrennt!	
              Hinweis: Puzzle mit sehr gro�en Werten 
                       k�nnen zu einer sehr langen Laufzeit
                       f�hren.
Bezeichnung - Die Bezeichnung ist optional
              Wird keine Beeichnung angegeben, erfolgt die Ausgabe 
              des Puzzles und der L�sung nacheinander auf dem Monitor.
              Wird eine Bezeichnung angegeben, werden die Dateien
	      "Raetsel <Bezeichnung>.txt" und
              "Musterloesung <Bezeichnung>.txt" 
              erzeugt.


RaetselTester
Der RaetselTester �berpr�ft ein �bergebenes Puzzle auf Korrektheit.
Damit haben Sie die M�glichkeit ihre selbsterzeugten L�sungen zu �berpr�fen.
Der Aufruf erfolgt �ber 

java -jar RaetselTester.jar <Dateiname>

Wird ein Fehler gefunden, wird die Zeile, in welcher der Fehler auftritt, 
sowie die Art des Fehlers angezeigt.


In den Ordnern 
"Beispiele" und "Musterl�sungen" stehen Ihnen die angegebenen Beispiele aus der Aufgabestellung zur Verf�gung. 

HINWEIS:
Die L�sung h�ngt stark von ihrem Algorithmus ab. Sollten Sie nicht auf die gleiche L�sung kommen wie die Musterl�sung, ist dies nicht verwunderlich.



Viel Erfolg.