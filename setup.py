from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
        name="holzpuzzle",
        python_requires='>3.7.0',
        version="1.0",
        author="Roman Romany",
        author_email="r.romany@fz-juelich.de",
        description="Großes Programm Holzpuzzle 2021",
        long_description=long_description,
        long_description_content_type="text/markdown",
        url="",
        packages=find_packages(),

        install_requires=[
            'pynvim',
            'argparse',
            'setuptools',
            # 'flask',
            # 'wheel',
            'numpy',
            ]
        )
